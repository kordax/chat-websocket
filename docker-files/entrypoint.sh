#!/bin/bash
set -e

export KAFKA_LISTENER=${KAFKA_LISTENER:-kafka.default.svc.cluster.local}

#Run application
cd /app
sbt "run -h 0.0.0.0 -p 8888 -k $KAFKA_LISTENER:9092" 2>/dev/null | egrep "^\{"
