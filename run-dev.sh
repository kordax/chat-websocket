#!/bin/bash

echo "Starting sbt with dev logging config"
sbt -Dlogback.configurationFile=src/main/resources/logback-dev.xml $@
