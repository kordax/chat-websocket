/*
 * [Config.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests.config

import com.redmadrobot.websocket.config.Defaults.CONFIG_HOST
import com.redmadrobot.websocket.config.Defaults.CONFIG_PORT

/**
  * Configuration case class.
  *
  * @param host host
  * @param port port
  * @param time load test time
  * @param connTout connection timeout
  * @param clients number of clients
  * @param requests number of requests in performance tests
  * @param delay request delay
  * @param dataFileName test data file name
  * @param prefix connection url prefix/protocol
  */
case class Config(host: String = CONFIG_HOST,
                  port: Int = CONFIG_PORT,
                  time: Int = Defaults.CONFIG_LOAD_TIME_S,
                  connTout: Int = Defaults.CONFIG_CONN_TIMEOUT_MS,
                  clients: Int = Defaults.CONFIG_NUM_OF_CLIENTS,
                  requests: Int = Defaults.CONFIG_NUM_OF_REQUESTS,
                  delay: Int = Defaults.CONFIG_REQ_DELAY_MS,
                  dataFileName: String = Defaults.CONFIG_TEST_DATA_FILE_NAME,
                  prefix: String = Defaults.CONFIG_CONNECTION_PREFIX,
                  suffix: String = Defaults.CONFIG_CONNECTION_SUFFIX,
                  user: String = Defaults.CONFIG_TEST_DATA_USER
                 )