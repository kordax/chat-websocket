/*
 * [Defaults.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests.config

/**
  * Default configuration values.
  *
  */
object Defaults {
  /** Default configuration load test time in seconds */
  val CONFIG_LOAD_TIME_S = 60
  /** Default configuration load test time in milliseconds */
  val CONFIG_CONN_TIMEOUT_MS = 30000
  /** Default configuration load test number of clients */
  val CONFIG_NUM_OF_CLIENTS = 10
  /** Default configuration load test number of requests per client */
  val CONFIG_NUM_OF_REQUESTS = 30
  /** Default configuration request delay in nanoseconds */
  val CONFIG_REQ_DELAY_MS = 0
  /** Default configuration connection URL prefix/protocol */
  val CONFIG_CONNECTION_PREFIX = "ws"
  /** Default configuration connection URI suffix/destination */
  val CONFIG_CONNECTION_SUFFIX = ""
  /** Default configuration test data file name */
  val CONFIG_TEST_DATA_FILE_NAME = "src/test/resources/test-data.json"
  /** Default configuration test data user */
  val CONFIG_TEST_DATA_USER = "a0000000-0000-3000-a000-000000000000"
}