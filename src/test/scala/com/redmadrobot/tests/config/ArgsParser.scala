/*
 * [ArgsParser.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests.config

import buildinfopkg.BuildInfo
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
  * Tests arguments parser.
  */
object ArgsParser {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  /** Invalid characters for argument value */
  private val HOST_INVALID_CHARS = Array("|/\\.!@#$%^&*()_+=-§±?><}{[]:;'\"~`".toCharArray)

  /** Current config */
  private var config = None: Option[Config]

  /** Current parser implementation */
  private val parser = new scopt.OptionParser[Config](BuildInfo.name) {
    head("""EXAMPLE: sbt testOnly * -- -Dh=10.110.10.207 -Dp=8080 -Dt=60 -Dc=120000 -Dd=500000 -Dn=500""")

    opt[String]('h', "host").action((x, c) => c.copy(host = x)).text("host or ip addr to connect to")
    opt[Int]('p', "port").action((x, c) => c.copy(port = x)).text("port to connect to")
    opt[Int]('c', "conn").action((x, c) => c.copy(connTout = x)).text("connection timeout in milliseconds")
    opt[Int]('t', "time").action((x, c) => c.copy(time = x)).text("load test time in seconds")
    opt[Int]('n', "clients").action((x, c) => c.copy(clients = x)).text("number of clients in connection/load tests")
    opt[Int]('r', "requests").action((x, c) => c.copy(requests = x)).text("number of requests per client in load/performance tests")
    opt[Int]('d', "delay").action((x, c) => c.copy(delay = x)).text("delay between requests in milliseconds")
    opt[String]('f', "file").action((x, c) => c.copy(dataFileName = x)).text("test data file name for test data generator")
    opt[Boolean]('s', "secure").action((x, c) => c.copy(prefix = "wss")).text("set to use HTTPS protocol")
    opt[String]('a', "append").action((x, c) => c.copy(suffix = x)).text("append suffix/destination to connection URI")
    opt[String]('u', "user").action((x, c) => c.copy(user = x)).text("test data generator user id")

    help("help").text("""EXAMPLE: sbt "testOnly * -- -Dh=10.110.10.207 -Dp=8080 -Dt=60 -Dc=120000 -Dd=15 -Dn=500 -Ds" """)
    version(BuildInfo.version)
  }

  /**
    * Parses given arguments into internal config
    *
    * @param args arguments
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def parse(args: Array[String]): Unit = {
    LOGGER.info("Parsing application arguments: " + args.toIterable.toSeq.toString())
    parser.parse(args, Config()) match {
      case Some(parsedConf) =>
        config = Some(parsedConf)
        LOGGER.debug("Args passed: " + parsedConf)
      case None => throw new IllegalArgumentException("Passed arguments are illegal")
    }
  }

  /**
    * Validates arguments
    *
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def validateArgs(): Unit = {
    LOGGER.debug("Validating application arguments: " + config)
    validateHost(config.get.host)
    validatePort(config.get.port)
  }

  /**
    * Get current config instance
    *
    * @return
    */
  def getConfig: Config = {
    LOGGER.debug("Getting config")
    config.get
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateHost(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      println("host argument contains invalid characters")
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }

  /**
    * Validates port
    *
    * @param port port
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePort(port: Int): Unit = {
    LOGGER.debug("Validating port: " + port)
    if (port < 1 || port > 65535) {
      LOGGER.error("port argument isn't valid")
      throw new IllegalArgumentException("port argument isn't valid")
    }
  }
}
