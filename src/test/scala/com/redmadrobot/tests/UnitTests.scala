/*
 * [Tests.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests

import java.text.SimpleDateFormat
import java.util.UUID

import com.redmadrobot.tests.config.Config
import com.redmadrobot.websocket.validation.RequestValidator
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.client.Request.EventLog
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.VALIDATION_ERROR
import net.arammeem.chatprotobuf.messages.client.Response.ValidationError.Code.{UNDEFINED_PARAMETER, VALUE_IS_BELOW_MINIMUM_VALUE}
import net.arammeem.chatprotobuf.messages.client.Response.{Error, ValidationError}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import org.scalatest._
import org.slf4j.LoggerFactory

/**
  * Integration tests class.
  */
class UnitTests extends FlatSpec with Matchers with BeforeAndAfter {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))

  private var config: Config = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  before {
    this.synchronized {
      tStart = System.currentTimeMillis()
      LOGGER.info(s"Starting test")
    }
  }

  after {
    this.synchronized {
      LOGGER.info(s"Test finished")
      LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
    }
  }

  "TEST_0001 Request validator" should "validate valid EVENT_LOG request" in {
    val requestValidator: RequestValidator = new RequestValidator
    assertResult(None) {requestValidator.validate(Request(Request.Type.EVENT_LOG, UUID.randomUUID().toString).withEventLog(EventLog(0L)), () => {5L})}
  }

  "TEST_0002 Request validator" should "fail on invalid EVENT_LOG request" in {
    val requestId = UUID.randomUUID().toString
    val requestValidator: RequestValidator = new RequestValidator
    val validationError = Response(Response.Type.ERROR, requestId, 5L)
      .withError(Error(VALIDATION_ERROR, Request.Type.EVENT_LOG)
          .withValidationError(ValidationError("startOffset", VALUE_IS_BELOW_MINIMUM_VALUE))
      )
    assertResult(Some(validationError)) {requestValidator.validate(Request(Request.Type.EVENT_LOG, requestId).withEventLog(EventLog(-1L)), () => {5L})}
  }

  "TEST_0003 Request validator" should "fail on request without oneOf provided" in {
    val requestId = UUID.randomUUID().toString
    val requestValidator: RequestValidator = new RequestValidator
    val validationError = Response(Response.Type.ERROR, requestId, 5L)
      .withError(Error(VALIDATION_ERROR, Request.Type.EVENT_LOG)
        .withValidationError(ValidationError("oneOf", UNDEFINED_PARAMETER))
      )
    assertResult(Some(validationError)) {requestValidator.validate(Request(Request.Type.CREATE_CONVERSATION, requestId), () => {5L})}
  }
}