/*
 * [Tests.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.UUID
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}

import akka.actor.ActorSystem
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import com.redmadrobot.websocket.jwt.{JwtHeaderEntry, JwtManager, JwtPayloadEntry}
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.thedeanda.lorem.{Lorem, LoremIpsum}
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.client.Request.{CreateConversation, CreateMessage, DeleteConversation, EventLog}
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.OFFSET_OUTDATED
import net.arammeem.chatprotobuf.messages.client.Response.Type.{CONVERSATION_UPDATED, MESSAGE_UPDATED}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import net.arammeem.chatprotobuf.messages.internal.internal.MessagePart.OneOf.Body
import net.arammeem.chatprotobuf.messages.internal.internal.{Conversation, MessagePart}
import org.scalatest._
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.TimeoutException
import scala.util.control.Breaks._

/**
  * Integration tests class.
  */
class PerformanceTests extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))

  private var config: Config = _
  private final val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  private var wsClient: WebSocketClient = _
  private var wsInterlocutorClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  private val lorem: Lorem = LoremIpsum.getInstance

  private final val convUsers: List[String] = List(
    "a0000000-0000-4000-b000-000000000000",
    "a0000000-0000-4000-e000-000000000000"
  )

  val jwtHeader = JwtHeaderEntry("RS256", "JWT")

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)

    connectTimeout.set(config.connTout)
    JwtManager.init()
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(args: Array[String]): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }

  /**
    * Initiate WebSocket connection.
    *
    * @param clientId client id
    * @param timeoutMS timeout in milliseconds
    * @param webSocketClient connection
    * @throws java.io.IOException if connection has failed
    * @return
    */
  @throws(classOf[IOException])
  private def connect(clientId: String, timeoutMS: Long, webSocketClient: WebSocketClient): Unit = {
    webSocketClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(clientId)))
    webSocketClient.connect(connectTimeout.get)
  }

  before {
    this.synchronized {
      tStart = System.currentTimeMillis()
      LOGGER.info(s"Starting test")
      wsClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("1"))
      wsInterlocutorClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("2"))
    }
  }

  after {
    this.synchronized {
      LOGGER.info(s"Test finished")
      LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
    }
  }

  "TEST_0001 WebSocket clients" should "send multiple messages into GROUP conversation in parallel" in {
    try {
      LOGGER.info(s"Starting benchmark test with ${config.clients} clients")

      val testConversationUsers: ArrayBuffer[String] = new ArrayBuffer[String](config.clients)

      // Prepare parallel clients
      val clients = Array.fill[WebSocketClient](config.clients) ({
        val client = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("perf_tests"))
        val userId = UUID.randomUUID().toString
        testConversationUsers += userId
        client.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(userId)))
        client
      })

      val parClients = clients.par
      parClients.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(Runtime.getRuntime.availableProcessors()))
      val n: AtomicLong = new AtomicLong(0L)
      val semaphore: Semaphore = new Semaphore(Runtime.getRuntime.availableProcessors())
      parClients.foreach(client => {
        semaphore.acquire()
        LOGGER.info(s"Connecting client number ${n.incrementAndGet()}")
        try {
          client.connect(connectTimeout.get)
        } catch {
          case e: Exception =>
            fail(e)
        }
        semaphore.release()
      })

      try {
        val t0 = System.currentTimeMillis()
        var passed = System.currentTimeMillis() - t0
        while (passed < 15000) {
          clients.foreach(client => assert(client.isConnected))
          passed = System.currentTimeMillis() - t0
        }
      } catch {
        case default: Exception =>
          LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
          fail(default)
      }

      // Create conversation
      LOGGER.info(s"Creating conversation")

      val createConversationRequestId = UUID.randomUUID.toString
      val createConversationRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConversationRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "Performance test conversation",
            None,
            testConversationUsers.toList
          )
        )

      clients(0).clearResponses()
      clients(0).sendToQueue(createConversationRequest)

      val createConversationResponse = clients(0).awaitResponse(config.connTout, createConversationRequestId, CONVERSATION_UPDATED)
      if (createConversationResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConversationResponse.toProtoString}")
      }

      // Start sending messages
      val conversationId = createConversationResponse.getConversationUpdated.conversation.get.id
      LOGGER.info(s"""Clients will send conversation messages to conversation with id "$conversationId"""")

      val body = com.google.protobuf.ByteString.copyFrom(lorem.getWords(1, 32), "UTF-8")
      val precachedRequest = Request(Request.Type.CREATE_MESSAGE, "")
        .withCreateMessage(
          CreateMessage(
            conversationId,
            parts = List(MessagePart("text", body.size(), Body(body)))
          )
        )

      val responseTimes: ArrayBuffer[Long] = new ArrayBuffer[Long](config.clients)
      val precachedRequestIds = Array.fill[String](config.clients * config.requests + 1) ({
        UUID.randomUUID().toString
      })

      val index: AtomicInteger = new AtomicInteger(0)
      val t0 = System.nanoTime()
      parClients.foreach(client => {
        for (i <- 0 until config.requests) {
          val requestId = precachedRequestIds(index.getAndIncrement())
          val ti0 = System.nanoTime()
          client.sendToQueue(precachedRequest.withId(requestId))

          val response = client.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
          if (response.`type`.equals(Response.Type.ERROR)) {
            fail(s"Error message received: ${response.toProtoString}")
          }
          val ti1 = System.nanoTime()
          responseTimes += (ti1 - ti0)
        }
      })
      val t1 = System.nanoTime()

      LOGGER.info(s"Number of parallel clients: ${config.clients}")
      LOGGER.info(s"Benchmark elapsed time: ${t1 - t0} ns, or ${(t1 - t0) / 1000000.0D} ms, or ${(t1 - t0) / 1000000000.0D} s")
      LOGGER.info(s"MEDIAN RESPONSE TIME (ms): ${getMedian(responseTimes) / 1000000.0D}, RATE (req/s): ${config.clients * config.requests * 1000000000.0D / (t1 - t0)}")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    }

    succeed
  }

  def getMedian(seq: Seq[Long]): Long = {
    val sortedSeq = seq.sortWith(_ < _)

    if (seq.size % 2 == 1) sortedSeq(sortedSeq.length / 2).longValue()
    else {
      val (up, down) = sortedSeq.splitAt(seq.length / 2)
      (up.last + down.head) / 2
    }
  }

  /**
    * Send EVENT_LOG request to update current state and wait for sync to occur.
    *
    * @param startOffset last known offset
    * @param lastOffset last offset to receive
    * @param timeoutMs timeout in milliseconds
    * @param client WebSocket client
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    * @throws OutdatedOffset if provided start offset is outdated
    * @throws SynchronizationError if unknown error has occurred
    */
  @throws(classOf[TimeoutException])
  @throws(classOf[OutdatedOffset])
  @throws(classOf[SynchronizationError])
  private def synchronizeToState(startOffset: Long, lastOffset: Long, timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.info(s"Synchronizing from $startOffset to $lastOffset")
    val eventLogRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending EVENT_LOG request: $eventLogRequestId")
    val request: Request = Request(Request.Type.EVENT_LOG, eventLogRequestId)
      .withEventLog(
        EventLog(startOffset)
      )

    var responses = ArrayBuffer[Response]()
    client.sendToQueue(request)

    LOGGER.info(s"Waiting for EVENT_LOG response with id: $eventLogRequestId")

    var lastOffsetReceived = false

    if (startOffset == lastOffset) {
      lastOffsetReceived = true
    }

    while (true) {
      var response = client.awaitResponse(timeoutMs)
      responses += response
      if (response.offset >= lastOffset) {
        lastOffsetReceived = true
      }

      if (response.id.equals(eventLogRequestId) && lastOffsetReceived) {
        if (response.`type`.equals(Response.Type.ERROR)) {
          if (response.getError.code.equals(OFFSET_OUTDATED)) {
            throw new OutdatedOffset
          } else {
            throw new SynchronizationError(s"Unknown error has occurred: ${response.getError.code.toString()}")
          }
        }

        return responses
      }
    }

    responses
  }

  /**
    * Send LAST_OFFSET and EVENT_LOG requests to update current state to last offset + 1 and wait for sync to occur.
    *
    * @param startOffset start offset
    * @param timeoutMs timeout in milliseconds
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    */
  @throws(classOf[TimeoutException])
  private def synchronizeFromState(startOffset: Long, timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.warn("================ SYNCHRONIZING TEST STATE ================")

    val lastOffsetRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending LAST_OFFSET request: $lastOffsetRequestId")
    val lastOffset: Request = Request(Request.Type.LAST_OFFSET, lastOffsetRequestId)

    client.sendToQueue(lastOffset)

    LOGGER.info(s"Awaiting LAST_OFFSET response with id: $lastOffsetRequestId")
    var lastOffsetResponse = client.awaitResponse(timeoutMs)
    while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
      lastOffsetResponse = client.awaitResponse(timeoutMs)
    }

    LOGGER.info(s"Last known offset for current client: ${lastOffsetResponse.offset}")
    synchronizeToState(startOffset, lastOffsetResponse.offset, timeoutMs, client)
  }

  /**
    * Send LAST_OFFSET and EVENT_LOG requests to update current state to last offset + 1 and wait for sync to occur.
    *
    * @param timeoutMs timeout in milliseconds
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    * @throws SynchronizationError if unknown error has occurred
    */
  @throws(classOf[TimeoutException])
  private def synchronize(timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.info("================ SYNCHRONIZING TEST STATE ================")

    val lastOffsetRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending LAST_OFFSET request: $lastOffsetRequestId")
    val lastOffset: Request = Request(Request.Type.LAST_OFFSET, lastOffsetRequestId)

    client.sendToQueue(lastOffset)

    LOGGER.info(s"Awaiting LAST_OFFSET response with id: $lastOffsetRequestId")
    var lastOffsetResponse = client.awaitResponse(timeoutMs)
    while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
      lastOffsetResponse = client.awaitResponse(timeoutMs)
    }

    LOGGER.info(s"Last known offset for current client: ${lastOffsetResponse.offset}")

    try {
      synchronizeToState(lastOffsetResponse.offset, lastOffsetResponse.offset, timeoutMs, client)
    } catch {
      case _: OutdatedOffset =>
        LOGGER.info(s"Provided offset ${lastOffsetResponse.offset} is outdated")
        while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
          lastOffsetResponse = client.awaitResponse(timeoutMs)
        }
        LOGGER.info(s"Using new one: ${lastOffsetResponse.offset}")
        synchronizeToState(lastOffsetResponse.offset, lastOffsetResponse.offset, timeoutMs, client)
      case e: SynchronizationError =>
        throw e
    }
  }

  /**
    * Delete conversation with provided id.
    *
    * @param conversationId id of conversation
    * @param client WebSocket client
    * @throws TimeoutException if timeout has occurred
    * @return response of deleting conversation
    */
  @throws(classOf[TimeoutException])
  private def deleteConversation(conversationId: String, client: WebSocketClient): Response = {
    val requestId = UUID.randomUUID.toString
    val request: Request = Request(Request.Type.DELETE_CONVERSATION, requestId)
      .withDeleteConversation(
        DeleteConversation(
          conversationId,
        )
      )

    client.clearResponses()
    client.sendToQueue(request)

    val response = client.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
    if (response.`type`.equals(Response.Type.ERROR)) {
      fail(s"Error message received: ${response.toProtoString}")
    }

    response
  }

  private class OutdatedOffset extends Exception {}
  private class SynchronizationError(private val message: String = "", private val cause: Throwable = None.orNull) extends Exception(message, cause)
}
