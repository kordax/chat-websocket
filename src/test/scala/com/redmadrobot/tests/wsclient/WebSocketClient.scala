/*
 * [WebSocketClient.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests.wsclient

import java.io.IOException
import java.util.UUID
import java.util.concurrent.LinkedBlockingQueue

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.ws._
import akka.stream._
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueueWithComplete}
import akka.util.ByteString
import net.arammeem.chatprotobuf.messages.client.Response.Type.ERROR
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import com.redmadrobot.websocket.common.Constants
import com.redmadrobot.websocket.util.VersionUtils
import com.typesafe.scalalogging.Logger

import scala.collection.mutable.ArrayBuffer
import scala.concurrent._
import scala.language.postfixOps

/**
  * AKKA-based WebSocket client wrapper.
  *
  * @param api api version string
  * @param prefix connection protocol prefix
  * @param suffix connection URI suffix/destination
  * @param host host
  * @param port port
  */
class WebSocketClient(val prefix: String, val suffix: String, val host: String, val port: Int, val sys: ActorSystem, api: String = VersionUtils.retrieveApiVersionString)(implicit LOGGER: Logger) {
  private var queue: SourceQueueWithComplete[Message] = _
  private var wsFlow: Flow[Message, Message, Future[WebSocketUpgradeResponse]] = _
  private var flow: Flow[Message, Message, Future[Done]] = _
  private var killSwitch: SharedKillSwitch = _

  private var connected: Boolean = false
  private val responses: LinkedBlockingQueue[Response] = new LinkedBlockingQueue[Response]
  private var jwtToken = ""
  private var offset: Long = 0L

  private implicit val system: ActorSystem = sys
  private implicit val executionContext: ExecutionContext = scala.concurrent.ExecutionContext.global
  private implicit val materializer: Materializer = ActorMaterializer()

  private val jwtParameterName = Constants.JWT_GET_PARAMETER_NAME
  private val apiVersionParameterName = Constants.API_VERSION_GET_PARAMETER_NAME
  private val appApiVersion = api

  /**
    * Initiate WebSocket connection.
    * This method waits for UPGRADE response. If response with UPGRADE header is received, then WebSocket connection
    * was successfully established.
    *
    * @param timeoutMS timeout in milliseconds
    * @throws IOException if connection has failed
    * @throws TimeoutException if connection has failed with timeout
    * @return
    */
  @throws(classOf[IOException])
  @throws(classOf[TimeoutException])
  def connect(timeoutMS: Long): Unit = {
    val connectionUrl = s"$prefix://$host:$port/$suffix"

    LOGGER.info(s"Connecting to $connectionUrl")
    LOGGER.debug(s"Using jwt token: $jwtToken")
    LOGGER.debug(s"Using api version: $appApiVersion")

    if (connected) {
      LOGGER.info("Already connected")

      return
    }

    val t0 = System.currentTimeMillis()

    killSwitch = KillSwitches.shared(UUID.randomUUID().toString)

    wsFlow = Http().webSocketClientFlow(WebSocketRequest(s"$connectionUrl?$jwtParameterName=$jwtToken&$apiVersionParameterName=$appApiVersion"),
      Http().defaultClientHttpsContext, None
    ).via(killSwitch.flow[Message])

    connected = false

    flow = wsFlow.mapMaterializedValue(upgradeResponse => upgradeResponse.flatMap {
      upgrade =>
        if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
          LOGGER.debug(s"Upgrade response status: ${upgrade.response.status}")
          LOGGER.info(s"Connection to $connectionUrl successfully established")
          connected = true

          Future.successful(Done)
        } else {
          LOGGER.error(s"Connection failed: [${upgrade.response.status}]")
          Future.failed(throw new IOException(s"Connection failed: [${upgrade.response.status}]"))

          throw new IOException(s"Connection failed: [${upgrade.response.status}]")
        }
    })

    queue = Source.queue[Message](1000000, OverflowStrategy.fail)
      .via(flow).toMat(Sink.foreach[Message](message => {
      try {
        message match {
          case msg: BinaryMessage =>
            if (msg.isStrict) {
              val response: Response = Response.parseFrom(message.asBinaryMessage.getStrictData.toArray)
              LOGGER.whenDebugEnabled({
                LOGGER.debug(s"Received response: ${response.toProtoString}")
                LOGGER.debug(s"Received response: ${response.`type`}, ${response.id}")
              })
              responses.add(response)
            } else {
              import scala.concurrent.duration._
              val toStrictFut = msg.toStrict(30 seconds)
              toStrictFut.map {s =>
                val response: Response = Response.parseFrom(s.getStrictData.toArray)

                LOGGER.whenDebugEnabled({
                  LOGGER.debug(s"Received response: ${response.toProtoString}")
                  LOGGER.debug(s"Received response: ${response.`type`}, ${response.id}")
                })
                responses.add(response)
              }
            }
          case default: Any => LOGGER.error(s"Unknown request structure received: ${default.toString}")
          }
      } catch {
        case default: Exception =>
          LOGGER.error(s"Failed to parse response: ${default.getMessage}")
      }
    }))(Keep.left).run()

    queue.watchCompletion().onComplete(f =>
    if (f.isSuccess) {
      LOGGER.info(s"WebSocket client successfully disconnected")
      this.connected = false
    } else {
      LOGGER.error(s"WebSocket client connection closure failed with exception: ${f.failed.get.getMessage}")
      this.connected = false
    })

    while (!connected) {
      if ((System.currentTimeMillis() - t0) > timeoutMS) {
        throw new TimeoutException("Timeout error while waiting for connection")
      }

      Thread.sleep(500)
    }
  }

  /**
    * Check client connection status.
    *
    * @return true or false
    */
  def isConnected: Boolean = {
    connected
  }

  /**
    * Disconnect.
    *
    * @param timeoutMS timeout in milliseconds
    * @throws TimeoutException if connection closure timeout has occurred
    */
  @throws(classOf[TimeoutException])
  def disconnect(timeoutMS: Int): Unit = {
    if (!connected) {
      LOGGER.info("Client is not connected")

      return
    }

    queue.complete()
    killSwitch.shutdown()

    val t0 = System.currentTimeMillis()
    while (this.connected) {
      if (System.currentTimeMillis() - t0 > timeoutMS) {
        this.responses.clear()
        throw new TimeoutException("Connection closure took too long")
      }
    }

    this.responses.clear()
  }

  /**
    * Send bytes to WebSocket flow.
    *
    * @param bytes bytes to send
    * @throws java.lang.RuntimeException if connection error occurred
    */
  def send(bytes: ByteString): Unit = {
    Http().singleWebSocketRequest(
      WebSocketRequest(s"ws://$host:$port/$suffix?$jwtParameterName=$jwtToken"),
      flow
    )
  }

  /**
    * Send bytes to WebSocket flow.
    *
    * @param request request
    * @throws java.lang.RuntimeException if connection error occurred
    */
  def sendToQueue(request: Request): Unit = {
    LOGGER.debug(s"Sending message to ws://$host:$port/$suffix\n${request.toProtoString}")
    queue offer BinaryMessage.Strict(ByteString(request.toByteArray))
  }

  /**
    * Send BinaryMessage to WebSocket flow (higher performance method).
    *
    * @param message bytes to send
    * @throws java.lang.RuntimeException if connection error occurred
    */
  def sendToQueue(message: BinaryMessage): Unit = {
    LOGGER.debug(s"Sending message to ws://$host:$port/$suffix")
    queue offer message
  }

  /**
    * Set JWT token.
    *
    * @param token JWT token
    */
  def setJWTToken(token: String): Unit = {
    jwtToken = token
  }

  /**
    * Get current log offset.
    *
    * @return offset
    */
  def getOffset: Long = {
    offset
  }

  /**
    * Await responses until timeout occurs.
    *
    * @param waitTime timeout in milliseconds
    * @return list of responses
    */
  def awaitResponses(waitTime: Int): ArrayBuffer[Response] = {
    LOGGER.debug(s"Awaiting for responses for next $waitTime milliseconds [jwtToken: $jwtToken]")
    val results: ArrayBuffer[Response] = new ArrayBuffer[Response](0)

    while (true) {
      try {
        results += awaitResponse(waitTime)
      } catch {
        case _: Throwable => return results
      }
    }

    results
  }

  /**
    * Await specific number of responses.
    *
    * @param responseNum number of responses to wait
    * @param timeoutMs timeout in milliseconds
    * @return list of responses
    */
  @throws(classOf[TimeoutException])
  def awaitResponses(responseNum: Int, timeoutMs: Int): ArrayBuffer[Response] = {
    LOGGER.debug(s"Awaiting for responses for next $timeoutMs milliseconds [jwtToken: $jwtToken]")
    val results: ArrayBuffer[Response] = new ArrayBuffer[Response](responseNum)

    while (results.length < responseNum) {
      try {
        results += awaitResponse(timeoutMs)
        LOGGER.debug("Received response")
      } catch {
        case _: Throwable => return results
      }
    }

    results
  }

  /**
    * Await single response with provided request/response id.
    *
    * @param timeoutMs timeout in milliseconds
    * @param requestId request id
    * @param responseType response type
    * @throws TimeoutException if timeout has occurred
    * @return response or error if response is of [[net.arammeem.chatprotobuf.messages.client.Response.Type.ERROR]] type
    */
  @throws(classOf[TimeoutException])
  def awaitResponse(timeoutMs: Int, requestId: String, responseType: Response.Type): Response = {
    LOGGER.whenDebugEnabled {
      LOGGER.debug(s"Awaiting for response of ${responseType.toString()} type with request id: $requestId")
    }
    val t0 = System.currentTimeMillis()

    while (true) {
      if ((System.currentTimeMillis() - t0) > timeoutMs) {
        throw new TimeoutException(s"Timeout reached while waiting for response: $requestId")
      }

      val response = responses.poll()

      if (response != null) {
        if (response.offset > offset) {
          offset = response.offset
        }

        if (response.id.equals(requestId) && response.`type`.equals(responseType)) {
          return response
        }

        if (response.`type`.equals(ERROR)) {
          LOGGER.error("Received error on await response")

          return response
        }
      }
    }

    Response()
  }

  /**
    * Await single response with provided predicate.
    *
    * @param timeoutMs timeout in milliseconds
    * @param predicate check, if it is our response
    * @throws TimeoutException if timeout has occurred
    * @return response
    */
  @throws(classOf[TimeoutException])
  def awaitResponse(timeoutMs: Int, predicate: Response => Boolean): Response = {
    LOGGER.debug(s"Awaiting for response for predicate")
    val t0 = System.currentTimeMillis()

    while (true) {
      if ((System.currentTimeMillis() - t0) > timeoutMs) {
        throw new TimeoutException(s"Timeout reached while waiting for response for predicate")
      }

      val response = responses.poll()

      if (response != null) {
        if (response.offset > offset) {
          offset = response.offset
        }

        if (predicate.apply(response)) {
          return response
        }
      }
    }

    Response()
  }

  /**
    * Await single response.
    * If already contains some responses, a list of responses will be returned immediately.
    * This private method updates actual offset.
    *
    * @param timeoutMs timeout in milliseconds
    * @throws TimeoutException if timeout has occurred
    * @return response
    */
  @throws(classOf[TimeoutException])
  def awaitResponse(timeoutMs: Int): Response = {
    LOGGER.debug(s"Awaiting for response [jwtToken: $jwtToken]")
    val t0 = System.currentTimeMillis()

      while (responses.isEmpty) {
        if ((System.currentTimeMillis() - t0) > timeoutMs) {
          throw new TimeoutException("Timeout reached while waiting for response")
        }
      }

      val response = responses.poll()

      if (response.offset > offset) {
        offset = response.offset
      }

      response
  }

  /**
    * Clear client responses queue.
    *
    */
  def clearResponses(): Unit = {
    responses.clear()
  }
}