/*
 * [Tests.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.UUID
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import net.arammeem.chatprotobuf.messages.client.Request.DeleteMessage.Mode.DELETE_FROM_CONVERSATION
import net.arammeem.chatprotobuf.messages.client.Request.{ConversationInfo, Conversations, CreateConversation, CreateMessage, DeleteConversation, DeleteMessage, DeleteMessages, EventLog, HideConversation, LeaveConversation, TypingActivity, UpdateConversation, UpdateMessage}
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.{CANNOT_UPDATE_USER_LIST_FOR_PEER_TO_PEER_CONVERSATION, OFFSET_OUTDATED}
import net.arammeem.chatprotobuf.messages.client.Response.MessageUpdated.Mode.{CREATED, UPDATED}
import net.arammeem.chatprotobuf.messages.client.Response.Type.{CONVERSATIONS, CONVERSATION_UPDATED, LEFT, MESSAGE_UPDATED, SERVICE}
import net.arammeem.chatprotobuf.messages.client.Response.{ConversationUpdated, MessageUpdated}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import net.arammeem.chatprotobuf.messages.internal.internal.Message.Status.{DELIVERED, READ, RECEIVED}
import net.arammeem.chatprotobuf.messages.internal.internal.MessagePart.OneOf.Body
import net.arammeem.chatprotobuf.messages.internal.internal._
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import com.redmadrobot.websocket.jwt.{JwtHeaderEntry, JwtManager, JwtPayloadEntry}
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.service.ServiceRequest
import net.arammeem.chatprotobuf.messages.service.ServiceRequest.SetUser
import net.arammeem.chatprotobuf.messages.service.ServiceRequest.Type.USER_UPDATED
import net.arammeem.chatprotobuf.messages.service.ServiceResponse.Type
import org.scalatest._
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.TimeoutException
import scala.util.control.Breaks._

/**
  * Integration tests class.
  */
class IntegrationTests extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))

  private var config: Config = _
  private final val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  private var wsClient: WebSocketClient = _
  private var wsInterlocutorClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  private val SUPPORT_CONVERSATION_KEY = "SUPPORT"
  private val SUPPORT_CONVERSATION_VALUE = "YES"

  // [0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}
  private var testUserId = "a0000000-0000-3000-a000-000000000000"
  private final val interlocutorUserId = "a0000000-0000-4000-a000-000000000000"
  private final val convUsers: List[String] = List(
    interlocutorUserId,
    "a0000000-0000-4000-b000-000000000000",
    "a0000000-0000-4000-e000-000000000000",
    testUserId
  )

  val jwtHeader = JwtHeaderEntry("RS256", "JWT")

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)
    testUserId = config.user

    connectTimeout.set(config.connTout)
    JwtManager.init()
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(args: Array[String]): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }

  /**
    * Initiate WebSocket connection.
    *
    * @param clientId client id
    * @param timeoutMS timeout in milliseconds
    * @param webSocketClient connection
    * @throws java.io.IOException if connection has failed
    * @return
    */
  @throws(classOf[IOException])
  private def connect(clientId: String, timeoutMS: Long, webSocketClient: WebSocketClient): Unit = {
    webSocketClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(clientId)))
    webSocketClient.connect(connectTimeout.get)
  }

  before {
    this.synchronized {
      tStart = System.currentTimeMillis()
      LOGGER.info(s"Starting test")
      wsClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("1"))
      wsInterlocutorClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("2"))
    }
  }

  after {
    this.synchronized {
      LOGGER.info(s"Test finished")
      LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
    }
  }

  "TEST_0001 WebSocket client" should "send conversations request" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)

      // Retrieve conversation info
      LOGGER.info("Retrieving conversation info")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.CONVERSATIONS, requestId)
        .withCurrentConversations(
          Conversations(
            1L,
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATIONS)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assertResult(CONVERSATIONS) {response.`type`}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0002 WebSocket client" should "connect and create GROUP conversation" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val requestId = UUID.randomUUID.toString
      val convTitle = "TEST_0001"
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val request: Request = Request(Request.Type.CREATE_CONVERSATION, requestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            convTitle,
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      assertResult(requestId) {response.id}
      convUsers.foreach(convUser => {
        assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {response.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {response.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {response.getConversationUpdated.mode}
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0002_1 WebSocket client" should "connect and create PEER_TO_PEER conversation" in {
    try {
      val senderId = "a0000000-0000-4000-b000-000000000000"
      connect(senderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomInterlocutor = UUID.randomUUID().toString

      val requestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val request: Request = Request(Request.Type.CREATE_CONVERSATION, requestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
            )),
            Seq(randomInterlocutor, senderId)
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        deleteConversation(response.getConversationUpdated.getConversation.id, wsClient)
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      assertResult(requestId) {response.id}
      assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(senderId))
      assertResult(Conversation.Type.PEER_TO_PEER) {response.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.CREATED) {response.getConversationUpdated.mode}

      val initialMessage = response.getConversationUpdated.createdFields.get.message.get
      assertResult(senderId) {initialMessage.authorId}
      assert(initialMessage.position >= 0)
      assertResult(body) {initialMessage.parts.head.getBody}

      deleteConversation(response.getConversationUpdated.getConversation.id, wsClient)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0003 WebSocket client" should "connect and create new message" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0003"
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            convTitle,
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createConvResponse.toProtoString}")

      assertResult(createConvRequestId) {createConvResponse.id}
      assertResult(Response.Type.CONVERSATION_UPDATED) {createConvResponse.`type`}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      val requestId = UUID.randomUUID.toString
      val contType = "TEST_0003"
      val messageBody = com.google.protobuf.ByteString.copyFrom("TestMessage_" + UUID.randomUUID().toString, "UTF-8")
      val request: Request = Request(Request.Type.CREATE_MESSAGE, requestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.getConversation.id,
            parts = List(MessagePart(contType, messageBody.size(), Body(messageBody)))
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      assertResult(requestId) {response.id}
      assertResult(MessageUpdated.Mode.CREATED) {response.getMessageUpdated.mode}
      assertResult(contType) {response.getMessageUpdated.getMessage.parts.head.contentType}
      assertResult(messageBody) {response.getMessageUpdated.getMessage.parts.head.getBody}
      assertResult(Message.Status.RECEIVED) {response.getMessageUpdated.getMessage.status}
      assertResult(response.offset) {createConvResponse.offset + 1L}
      assert(response.getMessageUpdated.getMessage.position > -1L, "Position value should be positive")
      assert(response.getMessageUpdated.getMessage.created > 0L, "Timestamp value should be positive")
      assert(response.getMessageUpdated.getMessage.updated > 0L, "Timestamp value should be positive")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0004 WebSocket client" should "connect and delete conversation" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0004"
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            convTitle,
            None,
            convUsers
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createConvResponse.toProtoString}")

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      LOGGER.info("Deleting conversation")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.DELETE_CONVERSATION, requestId)
        .withDeleteConversation(
          DeleteConversation(
            createConvResponse.getConversationUpdated.getConversation.id,
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      assertResult(requestId) {response.id}
      assertResult(ConversationUpdated.Mode.DELETED) {response.getConversationUpdated.mode}
      assertResult(convTitle) {response.getConversationUpdated.conversation.get.title}
      assertResult(createConvResponse.getConversationUpdated.conversation.get.id) {response.getConversationUpdated.conversation.get.id}

      // Check, that second user got event about Conversation deletion
      val conversationDeletionEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isConversationDeleted && response.id.equals(requestId))
      LOGGER.info(s"Received conversationDeletionEvent: ${conversationDeletionEvent.toProtoString}")
      assert(conversationDeletionEvent.getEvent.getConversationUpdated.getConversation.users.map(u => u.id).contains(interlocutorUserId))
      assertResult(convTitle) {conversationDeletionEvent.getEvent.getConversationUpdated.conversation.get.title}
      assertResult(createConvResponse.getConversationUpdated.conversation.get.id) {conversationDeletionEvent.getEvent.getConversationUpdated.conversation.get.id}
      assertResult(response.offset) {createConvResponse.offset + 1L}
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0005 WebSocket client" should "connect and delete message" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0005"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0005",
            None,
            convUsers
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Check, that another user got event about invitation to conversation
      val conversationUpdatedEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isAddedToConversation && response.id.equals(createConvRequestId))

      LOGGER.info(s"Received conversationUpdatedEvent: ${conversationUpdatedEvent.toProtoString}")
      assert(conversationUpdatedEvent.getEvent.getConversationUpdated.getConversation.users.map(u => u.id).contains(interlocutorUserId))

      // Create message in this conversation
      LOGGER.info("Creating message")

      val createMsgRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest)

      val createMsgResponse = wsClient.awaitResponse(config.connTout, createMsgRequestId, MESSAGE_UPDATED)
      if (createMsgResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse.toProtoString}")

      assertResult(createMsgRequestId) {createMsgResponse.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {createMsgResponse.`type`}
      assertResult(CREATED) {createMsgResponse.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse.getMessageUpdated.getMessage.parts.head.contentType}

      // Check, that another user got event about message creation
      val messageCreationEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isMessageCreated && response.id.equals(createMsgRequestId))

      LOGGER.info(s"Received messageCreationEvent: ${messageCreationEvent.toProtoString}")
      assertResult(CREATED) {messageCreationEvent.getEvent.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {messageCreationEvent.getEvent.getMessageUpdated.message.get.parts.head.contentType}

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.DELETE_MESSAGE, requestId)
        .withDeleteMessage(
          DeleteMessage(
            createMsgResponse.getMessageUpdated.getMessage.id,
            DELETE_FROM_CONVERSATION
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {response.`type`}
      assertResult(MessageUpdated.Mode.DELETED) {response.getMessageUpdated.mode}
      assertResult(response.offset) {createMsgResponse.offset + 1L}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      // Check, that another user got event about message deletion
      var messageDeletionEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isMessageDeleted && response.id.equals(requestId))

      LOGGER.info(s"Received messageDeletionEvent: ${messageDeletionEvent.toProtoString}")
      assertResult(MessageUpdated.Mode.DELETED) {messageDeletionEvent.getEvent.getMessageUpdated.mode}
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0006 WebSocket client" should "connect, create 10 conversations and receive complete history" in {
    try {
      connect("a0000000-0000-3000-a000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val preHistoricRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0006"
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val preHistoricRequest: Request = Request(Request.Type.CREATE_CONVERSATION, preHistoricRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            convTitle,
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      for (_ <- 1 to 10) {
        wsClient.sendToQueue(preHistoricRequest)
      }

      val preHistoricRequestResponse = wsClient.awaitResponse(config.connTout, preHistoricRequestId, CONVERSATION_UPDATED)
      if (preHistoricRequestResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${preHistoricRequestResponse.toProtoString}")
      }

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.EVENT_LOG, requestId)
        .withEventLog(
          EventLog(preHistoricRequestResponse.offset - 10)
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val responses = wsClient.awaitResponses(10, config.connTout)
      responses.foreach(response => {
        if (response.`type`.equals(Response.Type.ERROR)) {
          fail(s"Error message received: ${response.toProtoString}")
        }

        LOGGER.info(s"Received response: ${response.toProtoString}")
      })

      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0007_1 WebSocket client" should "retrieve GROUP conversation info" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0007_1"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0007_1",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Retrieve conversation info
      LOGGER.info("Retrieving conversation info")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.CONVERSATION_INFO, requestId)
        .withConversationInfo(
          ConversationInfo(
            createConvResponse.getConversationUpdated.getConversation.id,
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATIONS)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}

      val foundEntries = response.getConversations.conversationAndMessages.filter(entry =>
        entry.conversation.get.id.equals(createConvResponse.getConversationUpdated.getConversation.id)
      )

      assert(foundEntries.nonEmpty, "Retrieved conversations should contain created conversation id: " +
        s"${createConvResponse.getConversationUpdated.getConversation.id}")
      assertResult(response.offset) {createConvResponse.offset + 1L}

      val foundConversation = foundEntries.head.getConversation
      val millisInMinute = 60 * 1000
      val currentTimestamp = System.currentTimeMillis()
      assert(foundConversation.created - millisInMinute < currentTimestamp)
      assert(foundConversation.created + millisInMinute > currentTimestamp)
      assert(foundConversation.updated - millisInMinute < currentTimestamp)
      assert(foundConversation.updated + millisInMinute > currentTimestamp)

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0007_2 WebSocket client" should "retrieve P2P conversation info" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomUser = UUID.randomUUID().toString

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
            )),
            List(randomUser, "a0000000-0000-4000-b000-000000000000")
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomUser))
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains("a0000000-0000-4000-b000-000000000000"))
      })
      assertResult(Conversation.Type.PEER_TO_PEER) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult("") {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Retrieve conversation info
      LOGGER.info("Retrieving conversation info")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.CONVERSATION_INFO, requestId)
        .withConversationInfo(
          ConversationInfo(
            createConvResponse.getConversationUpdated.getConversation.id,
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATIONS)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}

      val foundEntries = response.getConversations.conversationAndMessages.filter(entry => {
        entry.conversation.get.id.equals(createConvResponse.getConversationUpdated.getConversation.id)
        entry.conversation.get.users.size == 2
      })

      assert(foundEntries.nonEmpty, "Retrieved conversations should contain created conversation id: " +
        s"${createConvResponse.getConversationUpdated.getConversation.id}")
      assertResult(response.offset) {createConvResponse.offset + 1L}

      val foundConversation = foundEntries.head.getConversation
      val millisInMinute = 60 * 1000
      val currentTimestamp = System.currentTimeMillis()
      assert(foundConversation.created - millisInMinute < currentTimestamp)
      assert(foundConversation.created + millisInMinute > currentTimestamp)
      assert(foundConversation.updated - millisInMinute < currentTimestamp)
      assert(foundConversation.updated + millisInMinute > currentTimestamp)

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0008 Two parallel WebSocket clients" should "update conversation and receive EVENT" in {
    try {
      val senderId = "a0000000-0000-4000-b000-000000000000"
      connect(senderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomInterlocutor = UUID.randomUUID().toString

      connect(randomInterlocutor, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)
      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0008",
            None,
            Seq(randomInterlocutor, senderId),
            scala.Option(ConversationMeta(Map((SUPPORT_CONVERSATION_KEY, SUPPORT_CONVERSATION_VALUE))))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(SUPPORT_CONVERSATION_VALUE) {createConvResponse.getConversationUpdated.getConversation.meta.get.values(SUPPORT_CONVERSATION_KEY)}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Retrieve conversation info
      LOGGER.info("Updating conversation info")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.UPDATE_CONVERSATION, requestId)
        .withUpdateConversation(
          UpdateConversation(
            createConvResponse.getConversationUpdated.getConversation.id,
            "updatedTitle",
            Seq(randomInterlocutor, senderId)
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(senderId))
      assertResult(Conversation.Type.GROUP) {response.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.UPDATED) {response.getConversationUpdated.mode}
      assertResult(response.offset) {createConvResponse.offset + 1L}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      // Check, that second user got event about Conversation updating
      val conversationUpdatedEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isConversationUpdated && response.id.equals(requestId))
      LOGGER.info(s"Received conversationUpdatedEvent: ${conversationUpdatedEvent.toProtoString}")

      assert(conversationUpdatedEvent.getEvent.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assertResult(Conversation.Type.GROUP) {conversationUpdatedEvent.getEvent.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.UPDATED) {conversationUpdatedEvent.getEvent.getConversationUpdated.mode}

      deleteConversation(createConvResponse.getConversationUpdated.getConversation.id, wsClient)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0008_1 Two parallel WebSocket clients" should "try to update P2P conversation users list and receive ERROR" in {
    try {
      val senderId = "a0000000-0000-4000-b000-000000000000"
      connect(senderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomInterlocutor = UUID.randomUUID().toString

      connect(randomInterlocutor, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)
      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Body text", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
            )),
            Seq(randomInterlocutor, senderId),
            None
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assertResult(Conversation.Type.PEER_TO_PEER) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Retrieve conversation info
      LOGGER.info("Updating conversation info")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.UPDATE_CONVERSATION, requestId)
        .withUpdateConversation(
          UpdateConversation(
            createConvResponse.getConversationUpdated.getConversation.id,
            "",
            Seq(UUID.randomUUID().toString, senderId)
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, Response.Type.ERROR)

      assertResult(Response.Type.ERROR) {response.`type`}
      assertResult(CANNOT_UPDATE_USER_LIST_FOR_PEER_TO_PEER_CONVERSATION) {response.getError.code}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      deleteConversation(createConvResponse.getConversationUpdated.getConversation.id, wsClient)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0009 WebSocket client" should "update message as DELIVERED" in {
    try {
      val currentSenderId = "a0000000-0000-4000-b000-000000000000"
      connect(currentSenderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0009"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0009",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Create message in this conversation
      LOGGER.info("Creating message")

      val createMsgRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest)

      val createMsgResponse = wsClient.awaitResponse(config.connTout, createMsgRequestId, MESSAGE_UPDATED)
      if (createMsgResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse.toProtoString}")

      assertResult(createMsgRequestId) {createMsgResponse.id}
      assertResult(CREATED) {createMsgResponse.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse.getMessageUpdated.getMessage.parts.head.contentType}
      assertResult(currentSenderId) {createMsgResponse.getMessageUpdated.getMessage.authorId}
      assert(createMsgResponse.getMessageUpdated.getMessage.created > 0)
      assert(createMsgResponse.getMessageUpdated.getMessage.updated > 0)

      // Retrieve conversation info
      LOGGER.info("Updating message")

      val requestId = UUID.randomUUID.toString
      val updatedBody = com.google.protobuf.ByteString.copyFrom("UpdatedBody", "UTF-8")
      val request: Request = Request(Request.Type.UPDATE_MESSAGE, requestId)
        .withUpdateMessage(
          UpdateMessage(
            createMsgResponse.getMessageUpdated.getMessage.id,
            DELIVERED,
            List(MessagePart("updatedMimeType", updatedBody.size(), Body(updatedBody)))
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {response.`type`}
      assertResult(createMsgResponse.getMessageUpdated.getMessage.id) {response.getMessageUpdated.getMessage.id}
      assertResult(DELIVERED) {response.getMessageUpdated.getMessage.status}
      assertResult("updatedMimeType") {response.getMessageUpdated.getMessage.parts.head.contentType}
      assertResult(currentSenderId) {response.getMessageUpdated.getMessage.authorId}
      assertResult(response.offset) {createMsgResponse.offset + 1L}
      assert(response.getMessageUpdated.getMessage.created > 0)
      assert(response.getMessageUpdated.getMessage.updated > 0)

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0009_1 WebSocket client" should "update message as READ" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)
      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0009_1"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0009_1",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Create message in this conversation
      LOGGER.info("Creating message")

      val createMsgRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest)

      val createMsgResponse = wsClient.awaitResponse(config.connTout, createMsgRequestId, MESSAGE_UPDATED)
      if (createMsgResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse.toProtoString}")

      assertResult(createMsgRequestId) {createMsgResponse.id}
      assertResult(CREATED) {createMsgResponse.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse.getMessageUpdated.getMessage.parts.head.contentType}

      // Retrieve conversation info
      LOGGER.info("Updating message")

      val requestId = UUID.randomUUID.toString
      val updatedBody = com.google.protobuf.ByteString.copyFrom("UpdatedBody", "UTF-8")
      val request: Request = Request(Request.Type.UPDATE_MESSAGE, requestId)
        .withUpdateMessage(
          UpdateMessage(
            createMsgResponse.getMessageUpdated.getMessage.id,
            READ,
            List(MessagePart("updatedMimeType", updatedBody.size(), Body(updatedBody))),
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assertResult(createMsgResponse.getMessageUpdated.getMessage.id) {response.getMessageUpdated.getMessage.id}
      assertResult(READ) {response.getMessageUpdated.getMessage.status}
      assertResult("updatedMimeType") {response.getMessageUpdated.getMessage.parts.head.contentType}
      assertResult(response.offset) {createMsgResponse.offset + 1L}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      // Check, that another user got event about message updating
      var messageUpdatingEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isMessageUpdated && response.id.equals(requestId))

      LOGGER.info(s"Received messageUpdatingEvent: ${messageUpdatingEvent.toProtoString}")
      assertResult(UPDATED) {messageUpdatingEvent.getEvent.getMessageUpdated.mode}
      assertResult(createMsgResponse.getMessageUpdated.getMessage.id) {messageUpdatingEvent.getEvent.getMessageUpdated.getMessage.id}
      assertResult(READ) {messageUpdatingEvent.getEvent.getMessageUpdated.getMessage.status}
      assertResult("updatedMimeType") {messageUpdatingEvent.getEvent.getMessageUpdated.getMessage.parts.head.contentType}
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0009_2 WebSocket client" should "update message as RECEIVED" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0009_2"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0009_2",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Create message in this conversation
      LOGGER.info("Creating message")

      val createMsgRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest)

      val createMsgResponse = wsClient.awaitResponse(config.connTout, createMsgRequestId, MESSAGE_UPDATED)
      if (createMsgResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse.toProtoString}")

      assertResult(createMsgRequestId) {createMsgResponse.id}
      assertResult(CREATED) {createMsgResponse.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse.getMessageUpdated.getMessage.parts.head.contentType}

      // Retrieve conversation info
      LOGGER.info("Updating message")

      val requestId = UUID.randomUUID.toString
      val updatedBody = com.google.protobuf.ByteString.copyFrom("UpdatedBody", "UTF-8")
      val request: Request = Request(Request.Type.UPDATE_MESSAGE, requestId)
        .withUpdateMessage(
          UpdateMessage(
            createMsgResponse.getMessageUpdated.getMessage.id,
            RECEIVED,
            List(MessagePart("updatedMimeType", updatedBody.size(), Body(updatedBody)))
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, MESSAGE_UPDATED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}
      assertResult(createMsgResponse.getMessageUpdated.getMessage.id) {response.getMessageUpdated.getMessage.id}
      assertResult(RECEIVED) {response.getMessageUpdated.getMessage.status}
      assertResult("updatedMimeType") {response.getMessageUpdated.getMessage.parts.head.contentType}
      assertResult(response.offset) {createMsgResponse.offset + 1L}

      LOGGER.info(s"Received response: ${response.toProtoString}")
      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0010 WebSocket clients with same user id" should "connect and create GROUP conversation" in {
    try {
      val clients = Array.fill[WebSocketClient](config.clients) ({
        val client = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem(UUID.randomUUID().toString))
        client.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry("a0000000-0000-4000-b000-000000000000")))
        client
      })

      val parClients = clients.par
      parClients.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(config.clients * 100))
      var n = 0L
      val semaphore = new Semaphore(config.clients - 1)
      parClients.foreach(client => {
        semaphore.acquire()
        n = n + 1
        LOGGER.info(s"Connecting client number $n")

        try {
          client.connect(connectTimeout.get)
        } catch {
          case e: Exception => fail(e)
        }
        semaphore.release()
      })

      clients.foreach(client => {
        assert(client.isConnected)
        semaphore.acquire()
        synchronize(connectTimeout.get, client)
        semaphore.release()
      })

      parClients.foreach(client => {
        semaphore.acquire()
        val requestId = UUID.randomUUID.toString
        val convTitle = "TEST_0010"
        val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
        val request: Request = Request(Request.Type.CREATE_CONVERSATION, requestId)
          .withCreateConversation(
            CreateConversation(
              Conversation.Type.GROUP,
              convTitle,
              None,
              convUsers
            )
          )

        client.clearResponses()
        val t0 = System.nanoTime()
        client.sendToQueue(request)

        val response = client.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
        if (response.`type`.equals(Response.Type.ERROR)) {
          fail(s"Error message received: ${response.toProtoString}")
        }

        LOGGER.info(s"Received response: ${response.toProtoString}")
        LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

        assertResult(requestId) {response.id}
        convUsers.foreach(convUser => {
          assert(response.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
        })
        assertResult(Conversation.Type.GROUP) {response.getConversationUpdated.getConversation.`type`}
        assertResult(convTitle) {response.getConversationUpdated.getConversation.title}
        assertResult(ConversationUpdated.Mode.CREATED) {response.getConversationUpdated.mode}
        semaphore.release()
      })
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0011 WebSocket client" should "connect and delete two messages" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0011"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0011",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Create messages in this conversation
      LOGGER.info("Creating first message")

      val createMsgRequestId1 = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest1: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId1)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest1)

      val createMsgResponse1 = wsClient.awaitResponse(config.connTout, createMsgRequestId1, MESSAGE_UPDATED)
      if (createMsgResponse1.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse1.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse1.toProtoString}")

      assertResult(createMsgRequestId1) {createMsgResponse1.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {createMsgResponse1.`type`}
      assertResult(CREATED) {createMsgResponse1.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse1.getMessageUpdated.getMessage.parts.head.contentType}

      LOGGER.info("Creating second message")

      val createMsgRequestId2 = UUID.randomUUID.toString
      val body2 = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest2: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId2)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.2", body2.size(), Body(body2)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest2)

      val createMsgResponse2 = wsClient.awaitResponse(config.connTout, createMsgRequestId2, MESSAGE_UPDATED)
      if (createMsgResponse2.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse2.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse2.toProtoString}")

      assertResult(createMsgRequestId2) {createMsgResponse2.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {createMsgResponse2.`type`}
      assertResult(CREATED) {createMsgResponse2.getMessageUpdated.mode}
      assertResult("merchant/toyou.2") {createMsgResponse2.getMessageUpdated.getMessage.parts.head.contentType}

      LOGGER.info("Delete messages")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.DELETE_MESSAGES, requestId)
        .withDeleteMessages(
          DeleteMessages(
            List(createMsgResponse1.getMessageUpdated.getMessage.id, createMsgResponse2.getMessageUpdated.getMessage.id),
            Request.DeleteMessages.Mode.DELETE_FROM_CONVERSATION
          )
        )

      wsInterlocutorClient.clearResponses()
      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, Response.Type.MESSAGES_DELETED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")

      assertResult(requestId) {response.id}
      assertResult(Response.Type.MESSAGES_DELETED) {response.`type`}
      assert(response.getMessagesDeleted.messageIds.contains(createMsgResponse1.getMessageUpdated.getMessage.id))
      assert(response.getMessagesDeleted.messageIds.contains(createMsgResponse2.getMessageUpdated.getMessage.id))
      assertResult(response.offset) {createMsgResponse2.offset + 1L}

      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")

      // Check, that another user got event about message deletion
      var messageUpdatingEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isMessageDeleted && response.id.equals(requestId))

      LOGGER.info(s"Received messageUpdatingEvent: ${messageUpdatingEvent.toProtoString}")
      assertResult(net.arammeem.chatprotobuf.messages.client.Response.MessageUpdated.Mode.DELETED) {messageUpdatingEvent.getEvent.getMessageUpdated.mode}
      assert(response.getMessagesDeleted.messageIds.contains(messageUpdatingEvent.getEvent.getMessageUpdated.getMessage.id))
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0012 WebSocket client" should "connect and hide two messages" in {
    try {
      connect("a0000000-0000-4000-b000-000000000000", connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0012"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0012",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(convTitle) {createConvResponse.getConversationUpdated.getConversation.title}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Create messages in this conversation
      LOGGER.info("Creating first message")

      val createMsgRequestId1 = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest1: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId1)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest1)

      val createMsgResponse1 = wsClient.awaitResponse(config.connTout, createMsgRequestId1, MESSAGE_UPDATED)
      if (createMsgResponse1.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse1.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse1.toProtoString}")

      assertResult(createMsgRequestId1) {createMsgResponse1.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {createMsgResponse1.`type`}
      assertResult(CREATED) {createMsgResponse1.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse1.getMessageUpdated.getMessage.parts.head.contentType}

      LOGGER.info("Creating second message")

      val createMsgRequestId2 = UUID.randomUUID.toString
      val body2 = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest2: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId2)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.2", body2.size(), Body(body2)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest2)

      val createMsgResponse2 = wsClient.awaitResponse(config.connTout, createMsgRequestId2, MESSAGE_UPDATED)
      if (createMsgResponse2.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse2.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse2.toProtoString}")

      assertResult(createMsgRequestId2) {createMsgResponse2.id}
      assertResult(Response.Type.MESSAGE_UPDATED) {createMsgResponse2.`type`}
      assertResult(CREATED) {createMsgResponse2.getMessageUpdated.mode}
      assertResult("merchant/toyou.2") {createMsgResponse2.getMessageUpdated.getMessage.parts.head.contentType}

      LOGGER.info("Hide messages")

      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.DELETE_MESSAGES, requestId)
        .withDeleteMessages(
          DeleteMessages(
            List(createMsgResponse1.getMessageUpdated.getMessage.id, createMsgResponse2.getMessageUpdated.getMessage.id),
            Request.DeleteMessages.Mode.HIDE_FOR_ME
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, Response.Type.MESSAGES_DELETED)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      LOGGER.info(s"Received response: ${response.toProtoString}")

      assertResult(requestId) {response.id}
      assertResult(Response.Type.MESSAGES_DELETED) {response.`type`}
      assert(response.getMessagesDeleted.messageIds.contains(createMsgResponse1.getMessageUpdated.getMessage.id))
      assert(response.getMessagesDeleted.messageIds.contains(createMsgResponse2.getMessageUpdated.getMessage.id))
      assertResult(response.offset) {createMsgResponse2.offset + 1L}

      LOGGER.info(s"Elapsed time: ${System.nanoTime() - t0} ns, or ${(System.nanoTime() - t0) / 1000000L} ms, or ${(System.nanoTime() - t0) / 1000000000.0D} s")
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0012_1 WebSocket client" should "hide conversation and then unhide conversation" in {
    try {
      var senderId = "a0000000-0000-4000-b000-000000000000"
      connect(senderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomInterlocutor = UUID.randomUUID().toString

      connect(randomInterlocutor, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)
      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val messageBody = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", messageBody.size(), Body(messageBody)))
            )),
            List(randomInterlocutor, senderId)
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(randomInterlocutor))
      assertResult(Conversation.Type.PEER_TO_PEER) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // First user hides P2P conversation
      LOGGER.info("Hide conversation")

      val hideConversationId = UUID.randomUUID.toString
      val hideConversationRequest: Request = Request(Request.Type.HIDE_CONVERSATION, hideConversationId)
        .withHideConversation(
          HideConversation(
            createConvResponse.getConversationUpdated.getConversation.id
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(hideConversationRequest)

      val hideConversationResponse = wsClient.awaitResponse(config.connTout, hideConversationId, CONVERSATION_UPDATED)
      if (hideConversationResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${hideConversationResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${hideConversationResponse.toProtoString}")
      assertResult(ConversationUpdated.Mode.HIDDEN) {hideConversationResponse.getConversationUpdated.mode}

      // Check, that another user got event about hidden conversation
      var conversationHiddenEvent = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isConversationHidden && response.id.equals(hideConversationId))

      LOGGER.info(s"Received conversationHiddenEvent: ${conversationHiddenEvent.toProtoString}")

      // First user gets conversations. This conversation should not be present there
      LOGGER.info("Getting conversations")

      val conversationsRequestId = UUID.randomUUID.toString
      val conversationsRequestRequest: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId)
        .withCurrentConversations(
          Conversations(
            10L
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(conversationsRequestRequest)

      val conversationsResponse = wsClient.awaitResponse(config.connTout, conversationsRequestId, CONVERSATIONS)
      if (conversationsResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${conversationsResponse.toProtoString}")
      }

      assertResult(conversationsRequestId) {conversationsResponse.id}
      assert(!conversationsResponse.getConversations.conversationAndMessages.map(c => c.getConversation.id)
        .contains(createConvResponse.getConversationUpdated.getConversation.id))

      // Second user creates message in this conversation
      LOGGER.info("Creating message")

      val createMsgRequestId = UUID.randomUUID.toString
      val body = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
          )
        )

      wsInterlocutorClient.clearResponses()
      wsInterlocutorClient.sendToQueue(createMsgRequest)

      val createMsgResponse = wsInterlocutorClient.awaitResponse(config.connTout, createMsgRequestId, MESSAGE_UPDATED)
      if (createMsgResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse.toProtoString}")

      assertResult(createMsgRequestId) {createMsgResponse.id}
      assertResult(CREATED) {createMsgResponse.getMessageUpdated.mode}
      assertResult("merchant/toyou.1") {createMsgResponse.getMessageUpdated.getMessage.parts.head.contentType}

      // First user gets conversations. This conversation should be present there
      LOGGER.info("Getting conversations second time")

      val conversationsRequestId2 = UUID.randomUUID.toString
      val conversationsRequestRequest2: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId2)
        .withCurrentConversations(
          Conversations(
            10L
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(conversationsRequestRequest2)

      val conversationsResponse2 = wsClient.awaitResponse(config.connTout, conversationsRequestId2, CONVERSATIONS)
      if (conversationsResponse2.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${conversationsResponse2.toProtoString}")
      }

      LOGGER.info(s"Received response: ${conversationsResponse2.toProtoString}")

      assertResult(conversationsRequestId2) {conversationsResponse2.id}
      assert(conversationsResponse2.getConversations.conversationAndMessages.map(c => c.getConversation.id)
        .contains(createConvResponse.getConversationUpdated.getConversation.id))

      // First user creates message in this conversation
      LOGGER.info("First user creates message")

      val createMsgRequestId2 = UUID.randomUUID.toString
      val body2 = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createMsgRequest2: Request = Request(Request.Type.CREATE_MESSAGE, createMsgRequestId2)
        .withCreateMessage(
          CreateMessage(
            createConvResponse.getConversationUpdated.conversation.get.id,
            None,
            List(MessagePart("merchant/toyou.1", body2.size(), Body(body2)))
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createMsgRequest2)

      val createMsgResponse2 = wsClient.awaitResponse(config.connTout, createMsgRequestId2, MESSAGE_UPDATED)
      if (createMsgResponse2.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createMsgResponse2.toProtoString}")
      }

      LOGGER.info(s"Received response: ${createMsgResponse2.toProtoString}")

      assertResult(createMsgRequestId2) {createMsgResponse2.id}
      assertResult(CREATED) {createMsgResponse2.getMessageUpdated.mode}

      deleteConversation(createConvResponse.getConversationUpdated.conversation.get.id, wsClient)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0013_1 WebSocket client" should "create conversation and leave from it" in {
    try {
      val conversationOwnerUserId = "a0000000-0000-4000-b000-000000000000"
      connect(conversationOwnerUserId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0011"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0011",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // Leave from conversation
      LOGGER.info("Leave from conversation")

      val leaveConversationRequestId = UUID.randomUUID.toString
      val leaveConversationRequest: Request = Request(Request.Type.LEAVE_FROM_CONVERSATION, leaveConversationRequestId)
        .withLeaveConversation(
          LeaveConversation(
            createConvResponse.getConversationUpdated.conversation.get.id,
          )
        )

      wsInterlocutorClient.clearResponses()
      wsInterlocutorClient.sendToQueue(leaveConversationRequest)

      // Check, that user got right response
      val leaveConversationResponse = wsInterlocutorClient.awaitResponse(config.connTout, leaveConversationRequestId, LEFT)
      if (leaveConversationResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${leaveConversationResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${leaveConversationResponse.toProtoString}")
      assertResult(createConvResponse.getConversationUpdated.conversation.get.id) {leaveConversationResponse.getLeft.conversationId}
      assertResult(interlocutorUserId) {leaveConversationResponse.getLeft.getUser.id}

      // Check, that another user got right event
      var messageUserLeftEvent = getResponseByPredicate(wsClient.getOffset, config.connTout, wsClient,
        response => response.`type`.isEvent && response.getEvent.`type`.isUserHasLeftConversation && response.id.equals(leaveConversationRequestId))

      LOGGER.info(s"Received userLeftEvent: ${messageUserLeftEvent.toProtoString}")
      assertResult(createConvResponse.getConversationUpdated.conversation.get.id) {messageUserLeftEvent.getEvent.getLeft.conversationId}
      assertResult(interlocutorUserId) {messageUserLeftEvent.getEvent.getLeft.user.get.id}

      // Check, that converastion has no left user
      LOGGER.info("Retrieving conversation info")
      val requestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.CONVERSATION_INFO, requestId)
        .withConversationInfo(
          ConversationInfo(
            createConvResponse.getConversationUpdated.getConversation.id,
          )
        )

      wsClient.clearResponses()
      val t0 = System.nanoTime()
      wsClient.sendToQueue(request)

      val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATIONS)
      if (response.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${response.toProtoString}")
      }

      assertResult(requestId) {response.id}

      val foundEntries = response.getConversations.conversationAndMessages.filter(entry =>
        entry.conversation.get.id.equals(createConvResponse.getConversationUpdated.getConversation.id)
      )

      assertResult(convUsers.size - 1) {foundEntries.head.getConversation.users.size}
      assert(!foundEntries.head.getConversation.users.map(u => u.id).contains(interlocutorUserId))
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0014_1 WebSocket client" should "create and update user" in {
    try {
      val conversationOwnerUserId = "a0000000-0000-4000-b000-000000000000"
      connect(conversationOwnerUserId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      // Create user
      LOGGER.info("Creating user")
      val createdUserId = UUID.randomUUID.toString

      val createUserRequestId = UUID.randomUUID.toString
      val createUserRequest: Request = Request(Request.Type.SERVICE, createUserRequestId)
        .withServiceRequest(
          ServiceRequest(
            USER_UPDATED
          ).withSetUser(
            SetUser(
              Some(User(createdUserId))
            )
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createUserRequest)

      val createUserResponse = wsClient.awaitResponse(config.connTout, createUserRequestId, SERVICE)
      if (createUserResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createUserResponse.toProtoString}")
      }

      // Check, that user is created
      assertResult(createUserRequestId) {createUserResponse.id}
      assertResult(Type.USER_UPDATED) {createUserResponse.getServiceResponse.`type`}
      assertResult(createdUserId) {createUserResponse.getServiceResponse.getUserUpdated.user.get.id}

      // Update user
      val userName = "userName"
      val userAvatarUrl = "avatarUrl"
      val userMetaKey = "userMetaKey"
      val userMetaValue = "userMetaValue"

      val updateUserRequestId = UUID.randomUUID.toString
      val updateUserRequest: Request = Request(Request.Type.SERVICE, updateUserRequestId)
        .withServiceRequest(
          ServiceRequest(
            USER_UPDATED
          ).withSetUser(
            SetUser(
              Some(User(createdUserId, userAvatarUrl, userName, scala.Option(UserMeta(Map((userMetaKey, userMetaValue))))))
            )
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(updateUserRequest)

      val updateUserResponse = wsClient.awaitResponse(config.connTout, updateUserRequestId, SERVICE)
      if (updateUserResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${updateUserResponse.toProtoString}")
      }

      // Check, that user is updated
      assertResult(updateUserRequestId) {updateUserResponse.id}
      assertResult(Type.USER_UPDATED) {updateUserResponse.getServiceResponse.`type`}
      assertResult(createdUserId) {updateUserResponse.getServiceResponse.getUserUpdated.user.get.id}
      assertResult(userName) {updateUserResponse.getServiceResponse.getUserUpdated.user.get.name}
      assertResult(userAvatarUrl) {updateUserResponse.getServiceResponse.getUserUpdated.user.get.avatarUrl}
      assertResult(userMetaValue) {updateUserResponse.getServiceResponse.getUserUpdated.user.get.meta.get.values(userMetaKey)}

    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0015_1 WebSocket client" should "hide conversation and then unhide conversation with explicit request" in {
    try {
      var senderId = "a0000000-0000-4000-b000-000000000000"
      connect(senderId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      val randomInterlocutor = UUID.randomUUID().toString

      // Create P2P conversation
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val messageBody = com.google.protobuf.ByteString.copyFrom("Nothing", "UTF-8")
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", messageBody.size(), Body(messageBody)))
            )),
            List(randomInterlocutor, senderId)
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      val initialMessage = createConvResponse.getConversationUpdated.createdFields.get.message.get
      assertResult(senderId) {initialMessage.authorId}
      assertResult(messageBody) {initialMessage.parts.head.getBody}

      // First user hides P2P conversation
      LOGGER.info("Hide conversation")

      val hideConversationId = UUID.randomUUID.toString
      val hideConversationRequest: Request = Request(Request.Type.HIDE_CONVERSATION, hideConversationId)
        .withHideConversation(
          HideConversation(
            createConvResponse.getConversationUpdated.getConversation.id
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(hideConversationRequest)

      val hideConversationResponse = wsClient.awaitResponse(config.connTout, hideConversationId, CONVERSATION_UPDATED)
      if (hideConversationResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${hideConversationResponse.toProtoString}")
      }

      LOGGER.info(s"Received response: ${hideConversationResponse.toProtoString}")
      assertResult(ConversationUpdated.Mode.HIDDEN) {hideConversationResponse.getConversationUpdated.mode}

      // First user gets conversations. This conversation should not be present there
      LOGGER.info("Getting conversations")

      val conversationsRequestId = UUID.randomUUID.toString
      val conversationsRequestRequest: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId)
        .withCurrentConversations(
          Conversations(
            10L
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(conversationsRequestRequest)

      val conversationsResponse = wsClient.awaitResponse(config.connTout, conversationsRequestId, CONVERSATIONS)
      if (conversationsResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${conversationsResponse.toProtoString}")
      }

      assertResult(conversationsRequestId) {conversationsResponse.id}
      assert(!conversationsResponse.getConversations.conversationAndMessages.map(c => c.getConversation.id)
        .contains(createConvResponse.getConversationUpdated.getConversation.id))

      // Explicit unhidden with CreateConversation request
      LOGGER.info("Unhide conversation")

      val unhideConvRequestId = UUID.randomUUID.toString
      val messageBody2 = com.google.protobuf.ByteString.copyFrom("Second message", "UTF-8")
      val unhideConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, unhideConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.PEER_TO_PEER,
            "",
            Some(CreateMessage(
              parts = List(MessagePart("merchant/toyou.1", messageBody2.size(), Body(messageBody2)))
            )),
            List(randomInterlocutor, senderId)
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(unhideConvRequest)

      val unhideConvResponse = wsClient.awaitResponse(config.connTout, unhideConvRequestId, CONVERSATION_UPDATED)
      if (unhideConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${unhideConvResponse.toProtoString}")
      }

      assertResult(ConversationUpdated.Mode.CREATED) {unhideConvResponse.getConversationUpdated.mode}
      val initialMessageFromUnhide = unhideConvResponse.getConversationUpdated.createdFields.get.message.get
      assertResult(senderId) {initialMessageFromUnhide.authorId}
      assertResult(messageBody2) {initialMessageFromUnhide.parts.head.getBody}

      assert(!initialMessage.id.equals(initialMessageFromUnhide.id))

      // First user gets conversations. Now, this conversation should be present there
      LOGGER.info("Getting conversations after unhide")

      val conversationsRequestId2 = UUID.randomUUID.toString
      val conversationsRequestRequest2: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId2)
        .withCurrentConversations(
          Conversations(
            10L
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(conversationsRequestRequest2)

      val conversationsResponse2 = wsClient.awaitResponse(config.connTout, conversationsRequestId2, CONVERSATIONS)
      if (conversationsResponse2.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${conversationsResponse2.toProtoString}")
      }

      assertResult(conversationsRequestId2) {conversationsResponse2.id}
      assert(conversationsResponse2.getConversations.conversationAndMessages.map(c => c.getConversation.id)
        .contains(createConvResponse.getConversationUpdated.getConversation.id))

      deleteConversation(createConvResponse.getConversationUpdated.conversation.get.id, wsClient)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
    }

    succeed
  }

  "TEST_0016_1 WebSocket client" should "send and receive type activity event" in {
    try {
      val conversationOwnerUserId = "a0000000-0000-4000-b000-000000000000"
      connect(conversationOwnerUserId, connectTimeout.get, wsClient)
      synchronize(connectTimeout.get, wsClient)

      connect(interlocutorUserId, connectTimeout.get, wsInterlocutorClient)
      synchronize(connectTimeout.get, wsInterlocutorClient)

      // Create group conversation before sending message
      LOGGER.info("Creating conversation")

      val createConvRequestId = UUID.randomUUID.toString
      val convTitle = "TEST_0016_1"
      val createConvRequest: Request = Request(Request.Type.CREATE_CONVERSATION, createConvRequestId)
        .withCreateConversation(
          CreateConversation(
            Conversation.Type.GROUP,
            "TEST_0016_1",
            None,
            convUsers
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(createConvRequest)

      val createConvResponse = wsClient.awaitResponse(config.connTout, createConvRequestId, CONVERSATION_UPDATED)
      if (createConvResponse.`type`.equals(Response.Type.ERROR)) {
        fail(s"Error message received: ${createConvResponse.toProtoString}")
      }

      assertResult(createConvRequestId) {createConvResponse.id}
      convUsers.foreach(convUser => {
        assert(createConvResponse.getConversationUpdated.getConversation.users.map(u => u.id).contains(convUser))
      })
      assertResult(Conversation.Type.GROUP) {createConvResponse.getConversationUpdated.getConversation.`type`}
      assertResult(ConversationUpdated.Mode.CREATED) {createConvResponse.getConversationUpdated.mode}

      // First user sends type activity event
      LOGGER.info("Sending type activity event")

      val sendTypeActivityEventRequestId = UUID.randomUUID.toString
      val typeActivityRequest: Request = Request(Request.Type.TYPING_ACTIVITY, sendTypeActivityEventRequestId)
        .withTypingActivity(
          TypingActivity(
            createConvResponse.getConversationUpdated.conversation.get.id,
            Request.TypingActivity.Type.START
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(typeActivityRequest)

      // Second online users have got typeActivity event from first user
      val typingActivityResponse = getResponseByPredicate(wsInterlocutorClient.getOffset, config.connTout, wsInterlocutorClient,
        response => response.`type`.isTypingActivity && response.id.equals(sendTypeActivityEventRequestId))

      LOGGER.info(s"Received typingActivity response: ${typingActivityResponse.toProtoString}")

      assertResult(createConvResponse.getConversationUpdated.conversation.get.id) {typingActivityResponse.getTypingActivity.conversationId}
      assertResult(Response.TypingActivity.Type.START) {typingActivityResponse.getTypingActivity.`type`}
      assertResult(conversationOwnerUserId) {typingActivityResponse.getTypingActivity.userId}

    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    } finally {
      wsClient.disconnect(5000)
      wsInterlocutorClient.disconnect(5000)
    }

    succeed
  }

  /**
    * Send EVENT_LOG request to get all responses.
    *
    * @param knownOffset last known offset
    * @param timeoutMs timeout in milliseconds
    * @param client WebSocket client
    * @param predicate wait until predicate is true
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    */
  @throws(classOf[TimeoutException])
  private def getResponseByPredicate(knownOffset: Long, timeoutMs: Int, client: WebSocketClient, predicate: Response => Boolean): Response = {
    val eventLogRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending EVENT_LOG request: $eventLogRequestId")
    val request: Request = Request(Request.Type.EVENT_LOG, eventLogRequestId)
      .withEventLog(
        EventLog(knownOffset)
      )

    client.sendToQueue(request)

    LOGGER.info(s"Waiting for EVENT_LOG response with id: $eventLogRequestId")

    client.awaitResponse(timeoutMs, predicate)
  }

  /**
    * Send EVENT_LOG request to update current state and wait for sync to occur.
    *
    * @param startOffset last known offset
    * @param lastOffset last offset to receive
    * @param timeoutMs timeout in milliseconds
    * @param client WebSocket client
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    * @throws OutdatedOffset if provided start offset is outdated
    * @throws SynchronizationError if unknown error has occurred
    */
  @throws(classOf[TimeoutException])
  @throws(classOf[OutdatedOffset])
  @throws(classOf[SynchronizationError])
  private def synchronizeToState(startOffset: Long, lastOffset: Long, timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.info(s"Synchronizing from $startOffset to $lastOffset")
    val eventLogRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending EVENT_LOG request: $eventLogRequestId")
    val request: Request = Request(Request.Type.EVENT_LOG, eventLogRequestId)
      .withEventLog(
        EventLog(startOffset)
      )

    var responses = ArrayBuffer[Response]()
    client.sendToQueue(request)

    LOGGER.info(s"Waiting for EVENT_LOG response with id: $eventLogRequestId")

    var lastOffsetReceived = false

    if (startOffset == lastOffset) {
      lastOffsetReceived = true
    }

    while (true) {
      var response = client.awaitResponse(timeoutMs)
      responses += response
      if (response.offset >= lastOffset) {
        lastOffsetReceived = true
      }

      if (response.id.equals(eventLogRequestId) && lastOffsetReceived) {
        if (response.`type`.equals(Response.Type.ERROR)) {
          if (response.getError.code.equals(OFFSET_OUTDATED)) {
            throw new OutdatedOffset
          } else {
            throw new SynchronizationError(s"Unknown error has occurred: ${response.getError.code.toString()}")
          }
        }

        return responses
      }
    }

    responses
  }

  /**
    * Send LAST_OFFSET and EVENT_LOG requests to update current state to last offset + 1 and wait for sync to occur.
    *
    * @param startOffset start offset
    * @param timeoutMs timeout in milliseconds
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    */
  @throws(classOf[TimeoutException])
  private def synchronizeFromState(startOffset: Long, timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.warn("================ SYNCHRONIZING TEST STATE ================")

    val lastOffsetRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending LAST_OFFSET request: $lastOffsetRequestId")
    val lastOffset: Request = Request(Request.Type.LAST_OFFSET, lastOffsetRequestId)

    client.sendToQueue(lastOffset)

    LOGGER.info(s"Awaiting LAST_OFFSET response with id: $lastOffsetRequestId")
    var lastOffsetResponse = client.awaitResponse(timeoutMs)
    while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
      lastOffsetResponse = client.awaitResponse(timeoutMs)
    }

    LOGGER.info(s"Last known offset for current client: ${lastOffsetResponse.offset}")
    synchronizeToState(startOffset, lastOffsetResponse.offset, timeoutMs, client)
  }

  /**
    * Send LAST_OFFSET and EVENT_LOG requests to update current state to last offset + 1 and wait for sync to occur.
    *
    * @param timeoutMs timeout in milliseconds
    * @return responses, given from last known offset
    * @throws TimeoutException if timeout has occurred
    * @throws SynchronizationError if unknown error has occurred
    */
  @throws(classOf[TimeoutException])
  private def synchronize(timeoutMs: Int, client: WebSocketClient): ArrayBuffer[Response] = {
    LOGGER.info("================ SYNCHRONIZING TEST STATE ================")

    val lastOffsetRequestId = UUID.randomUUID().toString
    LOGGER.info(s"Sending LAST_OFFSET request: $lastOffsetRequestId")
    val lastOffset: Request = Request(Request.Type.LAST_OFFSET, lastOffsetRequestId)

    client.sendToQueue(lastOffset)

    LOGGER.info(s"Awaiting LAST_OFFSET response with id: $lastOffsetRequestId")
    var lastOffsetResponse = client.awaitResponse(timeoutMs)
    while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
      lastOffsetResponse = client.awaitResponse(timeoutMs)
    }

    LOGGER.info(s"Last known offset for current client: ${lastOffsetResponse.offset}")

    try {
      synchronizeToState(lastOffsetResponse.offset, lastOffsetResponse.offset, timeoutMs, client)
    } catch {
      case _: OutdatedOffset =>
        LOGGER.info(s"Provided offset ${lastOffsetResponse.offset} is outdated")
        while (!lastOffsetResponse.id.equals(lastOffsetRequestId)) {
          lastOffsetResponse = client.awaitResponse(timeoutMs)
        }
        LOGGER.info(s"Using new one: ${lastOffsetResponse.offset}")
        synchronizeToState(lastOffsetResponse.offset, lastOffsetResponse.offset, timeoutMs, client)
      case e: SynchronizationError =>
        throw e
    }
  }

  /**
    * Delete conversation with provided id.
    *
    * @param conversationId id of conversation
    * @param client WebSocket client
    * @throws TimeoutException if timeout has occurred
    * @return response of deleting conversation
    */
  @throws(classOf[TimeoutException])
  private def deleteConversation(conversationId: String, client: WebSocketClient): Response = {
    val requestId = UUID.randomUUID.toString
    val request: Request = Request(Request.Type.DELETE_CONVERSATION, requestId)
      .withDeleteConversation(
        DeleteConversation(
          conversationId,
        )
      )

    client.clearResponses()
    client.sendToQueue(request)

    val response = client.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)
    if (response.`type`.equals(Response.Type.ERROR)) {
      fail(s"Error message received: ${response.toProtoString}")
    }

    response
  }

  private class OutdatedOffset extends Exception {}
  private class SynchronizationError(private val message: String = "", private val cause: Throwable = None.orNull) extends Exception(message, cause)
}
