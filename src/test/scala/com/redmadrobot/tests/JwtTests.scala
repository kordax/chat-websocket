/*
 * [ConnectionTests.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 12 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.websocket.jwt.{JwtHeaderEntry, JwtManager, JwtPayloadEntry}
import com.typesafe.scalalogging.Logger
import org.scalatest._
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext
import scala.util.control.Breaks._

/**
  * JWT tests class.
  */
class JwtTests extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))
  implicit val executionContext: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wsclient-akka-system")
  implicit val materializer: Materializer = ActorMaterializer()

  private var config: Config = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  val jwtHeader = JwtHeaderEntry("RS256", "JWT")

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)

    JwtManager.init()
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(args: Array[String]): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }

  before {
    tStart = System.currentTimeMillis()
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
  }

  after {
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
    LOGGER.info(s"Test finished at: ${df.format(new Date())}")
    LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
  }

  "JWT_TEST_0001 JWT manager" should "create tokens" in {
    val iosUsers: List[String] = List(
      "5fc82890-52f2-4839-9ce9-a57c4cb43ccc",
      "eb59f3c4-db0c-4405-80e2-73ff2f20ad40",
      "058c1fdf-a3c1-4868-8d46-a3bc75661688"
    )
    val jwtHeader = JwtHeaderEntry("RS256", "JWT")

    val user1Jwt = JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry("5fc82890-52f2-4839-9ce9-a57c4cb43ccc"))
    val user2Jwt = JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry("eb59f3c4-db0c-4405-80e2-73ff2f20ad40"))
    val user3Jwt = JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry("058c1fdf-a3c1-4868-8d46-a3bc75661688"))

    succeed
  }
}