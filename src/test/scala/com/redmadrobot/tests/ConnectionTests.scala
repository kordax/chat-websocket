/*
 * [ConnectionTests.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 12 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.tests

import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.{Semaphore, TimeoutException}

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import com.redmadrobot.websocket.jwt.{JwtHeaderEntry, JwtManager, JwtPayloadEntry}
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.typesafe.scalalogging.Logger
import org.scalatest._
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.ExecutionContext
import scala.util.control.Breaks._

/**
  * Main tests class.
  */
class ConnectionTests extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))
  implicit val executionContext: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wsclient-akka-system")
  implicit val materializer: Materializer = ActorMaterializer()

  private var config: Config = _
  private final val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  private var wsClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  val jwtHeader = JwtHeaderEntry("RS256", "JWT")

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)

    connectTimeout.set(config.connTout)
    JwtManager.init()
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(args: Array[String]): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }

  before {
    tStart = System.currentTimeMillis()
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
  }

  after {
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
    LOGGER.info(s"Test finished at: ${df.format(new Date())}")
    LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
    wsClient.clearResponses()
  }

  "TEST_0001 WebSocket client" should "connect" in {
    wsClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("wsclient-akka-system-1"))
    wsClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(config.user)))
    wsClient.connect(connectTimeout.get)
    wsClient.disconnect(config.connTout)

    succeed
  }

  s"TEST_0002 Multiple WebSocket clients" should "connect and respond for 15 seconds" in {
    LOGGER.info(s"${config.clients} simultaneous clients will be used")

    val clients = Array.fill[WebSocketClient](config.clients)({
      val client = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("wsclient-akka-system-2"))
      client.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(config.user)))
      client
    })

    val parClients = clients.par
    parClients.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(config.clients * 100))
    var n = 0L
    val semaphore: Semaphore = new Semaphore(config.clients)
    parClients.foreach(client => {
      semaphore.acquire()
      n = n + 1
      LOGGER.info(s"Connecting client number $n")
      client.connect(connectTimeout.get)
      semaphore.release()
    })

    try {
      val t0 = System.currentTimeMillis()
      var passed = System.currentTimeMillis() - t0

      while (passed < 15000) {
        clients.foreach(client => assert(client.isConnected))
        passed = System.currentTimeMillis() - t0
      }
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    }

    clients.foreach(client => {
      semaphore.acquire()
      client.disconnect(config.connTout)
      semaphore.release()
    })

    while (semaphore.getQueueLength > 0) {
      Thread.sleep(500)
    }

    succeed
  }

  "TEST_0003 WebSocket client" should "fail with invalid api version provided" in {
    wsClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem("wsclient-akka-system-1"), "2.17")
    wsClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(config.user)))
    assertThrows[TimeoutException] {wsClient.connect(connectTimeout.get)}
    wsClient.disconnect(config.connTout)

    succeed
  }
}