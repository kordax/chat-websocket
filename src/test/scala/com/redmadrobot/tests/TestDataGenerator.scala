package com.redmadrobot.tests

import java.io.{FileNotFoundException, IOException}
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import com.redmadrobot.websocket.jwt.{JwtHeaderEntry, JwtManager, JwtPayloadEntry}
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.client.Request.{Conversations, CreateConversation, CreateMessage}
import net.arammeem.chatprotobuf.messages.client.Response.ConversationUpdated
import net.arammeem.chatprotobuf.messages.client.Response.Conversations.ConversationAndMessages
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.CONVERSATION_ALREADY_EXISTS
import net.arammeem.chatprotobuf.messages.client.Response.Type.{CONVERSATIONS, CONVERSATION_UPDATED, ERROR}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import net.arammeem.chatprotobuf.messages.internal.internal.MessagePart.OneOf.Body
import net.arammeem.chatprotobuf.messages.internal.internal.{Conversation, MessagePart, User, UserMeta}
import org.scalatest._
import org.slf4j.LoggerFactory
import scalapb.json4s.JsonFormat
import spray.json.{JsValue, _}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.control.Breaks.{break, breakable} // if you don't supply your own Protocol (see below)

/**
  *
  */
class TestDataGenerator extends FlatSpec with Matchers with BeforeAndAfterAllConfigMap {
  private implicit final val LOGGER: Logger = Logger(LoggerFactory.getLogger(this.getClass))
  private final val TEST_DATA_DEFAULT_FILE_NAME = "src/test/resources/test-data.json"

  private var config: Config = _
  private final val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  private var wsClient: WebSocketClient = _

  private var TEST_USER_ID = ""
  private var CONV_USERS: ArrayBuffer[String] = ArrayBuffer(
    "eb59f3c4-db0c-4405-80e2-73ff2f20ad40",
    "058c1fdf-a3c1-4868-8d46-a3bc75661688",
  )

  import com.thedeanda.lorem.{Lorem, LoremIpsum}

  private val lorem: Lorem = LoremIpsum.getInstance

  private final val CONV_TITLES: List[String] = List(
    "Conv_01_GROUP_0_MESSAGES_1_USERS",
    "Conv_02_GROUP_0_MESSAGES_3_USERS",
    "Conv_03_GROUP_1_MESSAGE_1_USERS",
    "Conv_04_GROUP_1_MESSAGE_3_USERS",
    "Conv_05_GROUP_100_MESSAGES_1_USERS",
    "Conv_06_GROUP_100_MESSAGES_3_USERS",
    "Conv_07_P2P_0_MESSAGES_2_USERS",
    "Conv_08_P2P_1_MESSAGE_2_USERS",
    "Conv_09_P2P_100_MESSAGES_2_USERS",
    "Conv_10_P2P_0_MESSAGES_2_USERS",
    "Conv_11_P2P_1_MESSAGE_2_USERS",
    "Conv_12_P2P_100_MESSAGES_2_USERS",
    "Conv_13_P2P_0_MESSAGES_2_USERS",
    "Conv_14_P2P_1_MESSAGE_2_USERS",
    "Conv_15_P2P_100_MESSAGES_2_USERS",
    "Conv_16_GROUP_21_MESSAGES_1_USERS",
    "Conv_17_GROUP_10_MESSAGES_3_USERS",
    "Conv_18_GROUP_3_MESSAGES_1_USERS",
    "Conv_19_GROUP_5_MESSAGES_3_USERS",
    "Conv_20_GROUP_12_MESSAGES_1_USERS",
    "Conv_21_GROUP_2_MESSAGES_3_USERS",
  )

  private final val NUM_OF_MESSAGES = 100

  val jwtHeader = JwtHeaderEntry("RS256", "JWT")

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)
    TEST_USER_ID = config.user
    CONV_USERS += TEST_USER_ID

    connectTimeout.set(config.connTout)
    JwtManager.init()
  }

  "Test data generator" should s"generate test data in $TEST_DATA_DEFAULT_FILE_NAME" in {
    try {
      wsClient = new WebSocketClient(config.prefix, config.suffix, config.host, config.port, ActorSystem())
      wsClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(TEST_USER_ID)))
      connect(TEST_USER_ID, connectTimeout.get, wsClient)

      val conversationsRequestId = UUID.randomUUID.toString
      val request: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId)
        .withCurrentConversations(
          Conversations(
            NUM_OF_MESSAGES
          )
        )

      wsClient.clearResponses()
      wsClient.sendToQueue(request)

      var conversationsResponse: Response = Response()

      try {
        conversationsResponse = wsClient.awaitResponse(15000, conversationsRequestId, CONVERSATIONS)
        if (conversationsResponse.`type`.equals(Response.Type.ERROR)) {
          fail(s"Error message received: ${conversationsResponse.toProtoString}")
        }
      } catch {
        case _: Throwable => LOGGER.info("No conversations received")
      }

      var retrievedConversations: Seq[ConversationAndMessages] = conversationsResponse.getConversations.conversationAndMessages
      val uniqueConversations: ArrayBuffer[ConversationAndMessages] = ArrayBuffer()

      var diffProvided = false

      for (missingConversationTitle <- CONV_TITLES.filter(
        title => !retrievedConversations.exists(conversation => conversation.getConversation.title.equals(title))
      )) {
        breakable {
          diffProvided = true
          val requestId = UUID.randomUUID().toString
          val body = com.google.protobuf.ByteString.copyFrom(lorem.getWords(1, 64), "UTF-8")
          var users: List[String] = List()

          if (missingConversationTitle.contains("1_USERS")) {
            users = List(TEST_USER_ID)
          }

          if (missingConversationTitle.contains("2_USERS")) {
            val twoUsers: ArrayBuffer[String] = ArrayBuffer()

            if (missingConversationTitle.contains("P2P")) {
              twoUsers += TEST_USER_ID
              twoUsers += UUID.randomUUID().toString
            } else {
              twoUsers += TEST_USER_ID
              twoUsers += CONV_USERS.head
            }

            users = twoUsers.toList
          }

          if (missingConversationTitle.contains("3_USERS")) {
            users = CONV_USERS.toList
          }

          var convType = Conversation.Type.UNDEFINED_ENUM.value
          var request: Request = Request()

          if (missingConversationTitle.contains("P2P")) {
            convType = Conversation.Type.PEER_TO_PEER.value
            request = Request(Request.Type.CREATE_CONVERSATION, requestId)
              .withCreateConversation(
                CreateConversation(
                  Conversation.Type.PEER_TO_PEER,
                  "",
                  Some(CreateMessage(
                    parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
                  )),
                  users
                )
              )
          } else {
            convType = Conversation.Type.GROUP.value
            request = Request(Request.Type.CREATE_CONVERSATION, requestId)
              .withCreateConversation(
                CreateConversation(
                  Conversation.Type.GROUP,
                  missingConversationTitle,
                  None,
                  users
                )
              )
          }

          wsClient.sendToQueue(request)
          val response = wsClient.awaitResponse(config.connTout, requestId, CONVERSATION_UPDATED)

          if (response.`type`.equals(ERROR)) {
            if (response.getError.code.equals(CONVERSATION_ALREADY_EXISTS)) {
              LOGGER.info("Peer to peer conversation with this user already exists, skipping this one")
              break
            }
          }

          assertResult(requestId) {
            response.id
          }
          assertResult(convType) {
            response.getConversationUpdated.getConversation.`type`.value
          }
          assertResult(ConversationUpdated.Mode.CREATED) {
            response.getConversationUpdated.mode
          }
          assert(response.getConversationUpdated.getConversation.users.contains(User(TEST_USER_ID, meta = Some(UserMeta()))), s"Conversation should contain $TEST_USER_ID")

          if (missingConversationTitle.contains("_MESSAGES") && !missingConversationTitle.contains("P2P")) {
            val messagesNum = retrieveNumOfMessagesFromTitle(missingConversationTitle)
            for (_ <- 1 to messagesNum) {
              val reqId = UUID.randomUUID().toString
              val body = com.google.protobuf.ByteString.copyFrom(lorem.getWords(1, 64), "UTF-8")
              wsClient.sendToQueue(Request(Request.Type.CREATE_MESSAGE, reqId)
                .withCreateMessage(
                  CreateMessage(
                    response.getConversationUpdated.getConversation.id,
                    None,
                    parts = List(MessagePart("merchant/toyou.1", body.size(), Body(body)))
                  )
                )
              )
            }
          }
        }
      }

      if (diffProvided) {
        val conversationsRequestId = UUID.randomUUID.toString
        val request: Request = Request(Request.Type.CONVERSATIONS, conversationsRequestId)
          .withCurrentConversations(
            Conversations(
              NUM_OF_MESSAGES
            )
          )

        wsClient.clearResponses()
        wsClient.sendToQueue(request)

        val conversationsResponse = wsClient.awaitResponse(15000, conversationsRequestId, CONVERSATIONS)
        if (conversationsResponse.`type`.equals(Response.Type.ERROR)) {
          fail(s"Error message received: ${conversationsResponse.toProtoString}")
        }

        retrievedConversations = conversationsResponse.getConversations.conversationAndMessages
      }

      val existingConversations: Seq[ConversationAndMessages] = retrievedConversations.filter(conversation => CONV_TITLES.exists(title => conversation.getConversation.title.equals(title)))
      for (i <- existingConversations.indices) {
        breakable {
          for (n <- existingConversations.indices) {
            breakable {
              if (n == i) break
            }

            if (existingConversations(i).getConversation.title != existingConversations(n).getConversation.title) {
              if (!containsTitle(uniqueConversations, existingConversations(i).getConversation.title)) {
                uniqueConversations += existingConversations(i)
                break
              }
            }
          }
        }
      }

      LOGGER.info(s"Unique conversations:\n${uniqueConversations.map(entry => s"${entry.getConversation.title.padTo(40, ' ')}:${entry.getConversation.id}").mkString("\n")}")

      val dataFileJsValue = readData(TEST_DATA_DEFAULT_FILE_NAME)
      import spray.json._
      import DefaultJsonProtocol._ // if you don't supply your own Protocol (see below)

      if (dataFileJsValue.asJsObject.fields.contains(config.host) && dataFileJsValue.asJsObject.fields(config.host).convertTo[List[JsValue]].nonEmpty) {
        if (dataFileJsValue.asJsObject.fields.contains(config.user) && dataFileJsValue.asJsObject.fields(config.user).convertTo[List[JsValue]].nonEmpty) {
          LOGGER.info(s"Current host:user pair ('${config.host}:${config.user}') is already represented in test data")

          cancel(s"Current host:user pair ('${config.host}:${config.user}') is already represented in test data")
        }
      }

      val entries: mutable.HashMap[String, JsValue] = new mutable.HashMap[String, JsValue]
      import spray.json._

      var conversationsArray: Seq[JsValue] = Seq[JsValue]()
      uniqueConversations.foreach(conversation => {
        conversationsArray = conversationsArray :+ JsonFormat.toJsonString(conversation).parseJson
      })

      val array = JsArray(conversationsArray.toVector)

      val userArrayEntry: mutable.HashMap[String, JsValue] = new mutable.HashMap[String, JsValue]
      userArrayEntry.put(config.user, array)
      val hostArray = JsArray(JsObject(userArrayEntry.toMap))

      entries.put(config.host, hostArray)

      val jsValue: JsValue = JsObject(entries.toMap)
      val result = JsObject(dataFileJsValue.asJsObject.fields ++ jsValue.asJsObject.fields)
      updateData(TEST_DATA_DEFAULT_FILE_NAME, result)
    } catch {
      case default: Exception =>
        LOGGER.error(s"${default.getMessage}: ${LoggingHelpers.getStackTraceAsString(default)}")
        fail(default)
    }

    succeed
  }

  private def retrieveNumOfMessagesFromTitle(title: String): Int = {
    val numPattern = raw"(.*)_([0-9]{1,4}_MESSAGES)(.*)".r
    title match {
      case numPattern(_, num, _) =>
        return Integer.valueOf(num.substring(0, num.indexOf('_')))
      case _ =>
    }

    0
  }

  /**
    * Check if sequence of [[ConversationAndMessages]] already contains conversation title.
    *
    * @param sequence sequence
    * @param title conversation title
    * @return result
    */
  private def containsTitle(sequence: Seq[ConversationAndMessages], title: String): Boolean = {
    for (conversationAndMessages <- sequence) {
      if (conversationAndMessages.getConversation.title == title) return true
    }

    false
  }

  /**
    * Initiate WebSocket connection.
    *
    * @param clientId client id
    * @param timeoutMS timeout in milliseconds
    * @param webSocketClient connection
    * @throws java.io.IOException if connection has failed
    * @return
    */
  @throws(classOf[IOException])
  private def connect(clientId: String, timeoutMS: Long, webSocketClient: WebSocketClient): Unit = {
    webSocketClient.setJWTToken(JwtManager.generateJwtToken(jwtHeader, JwtPayloadEntry(clientId)))
    webSocketClient.connect(connectTimeout.get)
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(args: Array[String]): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }

  /**
    * Read data from .json file
    *
    * @param fileName file name
    * @return parsed data as Abstract Syntax Tree represented as [[JsValue]]
    * @throws DeserializationException if parsing error occurs
    * @throws IllegalArgumentException if any of provided arguments is invalid
    */
  //noinspection ScalaUnusedSymbol
  @throws(classOf[DeserializationException])
  @throws(classOf[IllegalArgumentException])
  private def readData(fileName: String): JsValue = {
    LOGGER.info(s"Reading test data from '$fileName'")

    if (fileName.isEmpty) {
      throw new IllegalArgumentException("File name is empty")
    }

    if (!fileName.endsWith(".json")) {
      throw new IllegalArgumentException("File name extension isn't of \'.json\' type")
    }

    val value = Source.fromFile(fileName).getLines.mkString.parseJson
    LOGGER.info(s"Read operation from '$fileName' succeeded")

    value
  }

  /**
    * Update data in .json file
    *
    * @param fileName file name
    * @return parsed data as Abstract Syntax Tree represented as [[JsValue]]
    * @throws FileNotFoundException if file doesn't exist
    * @throws IllegalArgumentException if any of provided arguments is invalid
    */
  @throws(classOf[FileNotFoundException])
  @throws(classOf[IllegalArgumentException])
  private def updateData(fileName: String, json: JsValue): Unit = {
    LOGGER.info(s"Updating test data in '$fileName'")

    if (fileName.isEmpty) {
      throw new IllegalArgumentException("File name is empty")
    }

    if (!fileName.endsWith(".json")) {
      throw new IllegalArgumentException("File name extension isn't of \'.json\' type")
    }

    import java.io._
    val pw = new PrintWriter(new File(fileName))
    pw.append(json.prettyPrint)
    pw.close()
    LOGGER.info(s"Test data successfully updated in '$fileName'")
  }
}
