/*
 * [Config.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.config

/**
  * Configuration case class.
  *
  * @param host host
  * @param port port
  * @param kafka kafka broker
  * @param assignTimeout partition assignment timeout
  * @param partition partition number
  * @param secure WebSocket over TLS flag
  * @param historyBound max history lag bound
  * @param onlineUsersCacheLifetimeInSeconds lifetime of redis online users cache
  * @param redisHost redis host
  * @param redisPort redis port
  * @param redisConnectionTimeout redis connection timeout
  * @param chatBackendHost chat-backend instance host to use to retrieve context
  * @param chatBackendPort chat-backend instance port to use to retrieve context
  * @param httpClientConnectionTimeout HTTP Client connection timeout
  * @param lingerMs queue thread linger in milliseconds
  * @param parallelism queue parallelism level
  */
case class Config(host: String = Defaults.CONFIG_HOST,
                  port: Int = Defaults.CONFIG_PORT,
                  kafka: String = Defaults.CONFIG_KAFKA_BROKER_LIST,
                  assignTimeout: Int = Defaults.CONFIG_KAFKA_ASSIGNMENT_TIMEOUT,
                  partition: Int = Defaults.CONFIG_KAFKA_ASSIGNMENT_PARTITION,
                  secure: Boolean = Defaults.CONFIG_IS_SECURE,
                  historyBound: Int = 1000,
                  onlineUsersCacheLifetimeInSeconds: Int = Defaults.CONFIG_REDIS_ONLINE_USERS_CACHE_LIFETIME,
                  redisHost: String = Defaults.CONFIG_REDIS_HOST,
                  redisPort: Int = Defaults.CONFIG_REDIS_PORT,
                  redisConnectionTimeout: Int = Defaults.CONFIG_REDIS_CONNECTION_TIMEOUT,
                  chatBackendHost: String = Defaults.CONFIG_CHAT_BACKEND_HOST,
                  chatBackendPort: Int = Defaults.CONFIG_CHAT_BACKEND_PORT,
                  httpClientConnectionTimeout: Int = Defaults.CONFIG_HTTP_CLIENT_CONNECTION_TIMEOUT,
                  lingerMs: Int = Defaults.CONFIG_QUEUE_THREAD_LINGER_MS,
                  parallelism: Int = Defaults.CONFIG_QUEUE_PARALLELISM
                 ) {

  override def toString: String = {
    s"host: $host\n" +
      s"port: $port\n" +
      s"kafka: $kafka\n" +
      s"assignTimeout: $assignTimeout\n" +
      s"partition: $partition\n" +
      s"secure: $secure\n" +
      s"historyBound: $historyBound\n" +
      s"onlineUsersCacheLifetimeInSeconds: $onlineUsersCacheLifetimeInSeconds\n" +
      s"redisHost: $redisHost\n" +
      s"redisPort: $redisPort\n" +
      s"redisConnectionTimeout: $redisConnectionTimeout\n" +
      s"chatBackendHost: $chatBackendHost\n" +
      s"chatBackendPort: $chatBackendPort\n" +
      s"httpClientConnectionTimeout: $httpClientConnectionTimeout\n" +
      s"lingerMs: $lingerMs\n" +
      s"parallelism: $parallelism"
  }
}