/*
 * [Defaults.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.config

import java.time.Duration

/**
  * Default configuration values.
  *
  */
object Defaults {
  /** Default configuration host */
  val CONFIG_HOST = "localhost"
  /** Default configuration port */
  val CONFIG_PORT = 8080
  /** Default kafka broker string */
  val CONFIG_KAFKA_BROKER_LIST = "kafka-host:9092"
  /** Default kafka partition assignment timeout in milliseconds */
  val CONFIG_KAFKA_ASSIGNMENT_TIMEOUT = 5000
  /** Default kafka partition assignment timeout in milliseconds */
  val CONFIG_KAFKA_ASSIGNMENT_PARTITION = 0
  /** Default secure connection boolean value */
  val CONFIG_IS_SECURE = false
  /** Default secure connection boolean value */
  val CONFIG_HISTORY_BOUND = 1000
  /** Default redis online users cache lifetime value */
  val CONFIG_REDIS_ONLINE_USERS_CACHE_LIFETIME: Int = Duration.ofMinutes(15).toSeconds.intValue()
  /** Default configuration redis host */
  val CONFIG_REDIS_HOST = "redis-host"
  /** Default configuration redis port */
  val CONFIG_REDIS_PORT = 6379
  /** Default redis timeout in milliseconds */
  val CONFIG_REDIS_CONNECTION_TIMEOUT = 2000
  /** Default configuration chat-backend host */
  val CONFIG_CHAT_BACKEND_HOST = "backend-host"
  /** Default configuration chat-backend port */
  val CONFIG_CHAT_BACKEND_PORT = 8080
  /** Default http client timeout in milliseconds */
  val CONFIG_HTTP_CLIENT_CONNECTION_TIMEOUT = 1000
  /** Default worker thread linger ms  */
  val CONFIG_QUEUE_THREAD_LINGER_MS = 1
  /** Default consumer queue parallelism level */
  val CONFIG_QUEUE_PARALLELISM = 3
}
