/*
 * [ArgsParser.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.config

import buildinfopkg.BuildInfo
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
  * Application arguments parser.
  */
object ArgsParser {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  /** Invalid characters for argument value */
  private val HOST_INVALID_CHARS = Array("|/\\.!@#$%^&*()_+=-§±?><}{[]:;'\"~`".toCharArray)

  /** Current config */
  private var config = None: Option[Config]

  /** Current parser implementation */
  private val parser = new scopt.OptionParser[Config]("""sbt run""") {
    head(BuildInfo.name, BuildInfo.version)

    opt[String]('h', "host").action((x, c) => c.copy(host = x)).text(s"host or ip addr to bind to (DEFAULT: ${Defaults.CONFIG_HOST})")
    opt[Int]('p', "port").action((x, c) => c.copy(port = x)).text(s"port to bind to (DEFAULT: ${Defaults.CONFIG_PORT})")
    opt[String]('k', "kafka").action((x, c) => c.copy(kafka = x)).text(s"kafka broker list (DEFAULT: ${Defaults.CONFIG_KAFKA_BROKER_LIST})")
    opt[Int]('n', "part").action((x, c) => c.copy(partition = x)).text(s"kafka partition (DEFAULT: ${Defaults.CONFIG_KAFKA_ASSIGNMENT_PARTITION})")
    opt[Int]('t', "timeout").action((x, c) => c.copy(assignTimeout = x)).text(s"kafka partition assignment timeout in milliseconds (DEFAULT: ${Defaults.CONFIG_KAFKA_ASSIGNMENT_TIMEOUT})")
    opt[Unit]('s', "secure").action( (_, c) => c.copy(secure = true) ).text("use secure WebSocket over TLS")
    opt[Int]('b', "bound").action( (x, c) => c.copy(historyBound = x) ).text("maximum client offset lag before receiving error response")
    opt[Int]("lifetime-user-cache").action( (x, c) => c.copy(onlineUsersCacheLifetimeInSeconds = x) ).text("lifetime of redis online users cache")
    opt[String]("redis-host").action((x, c) => c.copy(redisHost = x)).text(s"redis host or ip addr to bind to (DEFAULT: ${Defaults.CONFIG_REDIS_HOST})")
    opt[Int]("redis-port").action((x, c) => c.copy(redisPort = x)).text(s"redis port to bind to (DEFAULT: ${Defaults.CONFIG_REDIS_HOST})")
    opt[Int]('t', "redis-connection-timeout").action((x, c) => c.copy(redisConnectionTimeout = x)).text(s"Redis connection timeout in milliseconds (DEFAULT: ${Defaults.CONFIG_REDIS_CONNECTION_TIMEOUT})")
    opt[String]("chat-backend-host").action((x, c) => c.copy(chatBackendHost = x)).text(s"chat-backend host or ip addr to bind to (DEFAULT: ${Defaults.CONFIG_CHAT_BACKEND_HOST})")
    opt[Int]("chat-backend-port").action((x, c) => c.copy(chatBackendPort = x)).text(s"chat-backend port to bind to (DEFAULT: ${Defaults.CONFIG_CHAT_BACKEND_PORT})")
    opt[Int]('t', "http-client-connection-timeout").action((x, c) => c.copy(httpClientConnectionTimeout = x)).text(s"http-client connection timeout in milliseconds (DEFAULT: ${Defaults.CONFIG_HTTP_CLIENT_CONNECTION_TIMEOUT})")
    opt[Int]('l', "linger").action((x, c) => c.copy(lingerMs = x)).text(s"queue thread linger period in milliseconds (DEFAULT: ${Defaults.CONFIG_QUEUE_THREAD_LINGER_MS})")
    opt[Int]('q', "parallelism").action((x, c) => c.copy(lingerMs = x)).text(s"queue parallelism level (DEFAULT: ${Defaults.CONFIG_QUEUE_PARALLELISM})")
    help("help").text(s"""EXAMPLE: sbt "run -h 192.168.1.1 -p 8080 -k ${Defaults.CONFIG_KAFKA_BROKER_LIST}" """)
    version("version")
  }

  /**
    * Parses given arguments into internal config.
    *
    * @param args arguments
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def parse(args: Array[String]): Unit = {
    LOGGER.debug("Parsing application arguments: " + args.mkString)
    parser.parse(args, Config()) match {
      case Some(parsedConf) =>
        config = Some(parsedConf)
        LOGGER.debug("Args passed: " + parsedConf)
      case None => throw new IllegalArgumentException("Passed arguments are illegal")
    }
  }

  /**
    * Validates arguments
    *
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def validateArgs(): Unit = {
    LOGGER.debug("Validating application arguments: " + config)
    validateHost(config.get.host)
    validatePort(config.get.port)
    validatePartition(config.get.partition)
  }

  /**
    * Get current config instance
    *
    * @return
    */
  def getConfig: Config = {
    LOGGER.debug("Getting config")
    config.get
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateHost(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      println("host argument contains invalid characters")
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }

  /**
    * Validates port
    *
    * @param port port
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePort(port: Int): Unit = {
    LOGGER.debug("Validating port: " + port)
    if (port < 1 || port > 65535) {
      LOGGER.error("port argument isn't valid")
      throw new IllegalArgumentException("port argument isn't valid")
    }
  }

  /**
    * Validates partition
    *
    * @param partition partition
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePartition(partition: Int): Unit = {
    LOGGER.debug("Validating partition: " + partition)
    if (partition < 0) {
      LOGGER.error("partition argument isn't valid")
      throw new IllegalArgumentException("partition argument isn't valid")
    }
  }
}
