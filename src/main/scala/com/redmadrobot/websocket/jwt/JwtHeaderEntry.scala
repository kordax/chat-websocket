/*
 * [JwtHeaderEntry.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.jwt

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

/**
  * Jwt header entry case class.
  *
  * @param alg alg value
  * @param typ typ value
  */
final case class JwtHeaderEntry(alg: String, typ: String)

/**
  * Header entry protocol.
  */
object JwtHeaderEntryProtocol extends DefaultJsonProtocol {
  implicit val headerFormat: RootJsonFormat[JwtHeaderEntry] = jsonFormat2(JwtHeaderEntry)
}