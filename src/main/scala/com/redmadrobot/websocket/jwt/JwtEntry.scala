/*
 * [JwtEntry.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.jwt

/**
  * Jwt entry case class.
  *
  * @param jwtHeaderEntry jwt header
  * @param jwtPayloadEntry jwt payload
  * @param secret RS256 secret key
  */
final case class JwtEntry(jwtHeaderEntry: JwtHeaderEntry, jwtPayloadEntry: Option[JwtPayloadEntry], secret: String)