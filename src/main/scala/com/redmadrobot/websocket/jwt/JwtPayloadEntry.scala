/*
 * [JwtPayloadEntry.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.jwt

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

/**
  * Jwt payload entry case class.
  *
  * @param userId user id UUID value
  */
final case class JwtPayloadEntry(userId: String)

/**
  * Payload entry protocol.
  */
object JwtPayloadEntryProtocol extends DefaultJsonProtocol {
  implicit val payloadFormat: RootJsonFormat[JwtPayloadEntry] = jsonFormat1(JwtPayloadEntry)
}