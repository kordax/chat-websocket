/*
 * [Jwt.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.jwt

import java.io.{BufferedInputStream, FileInputStream, FileNotFoundException}
import java.security.{GeneralSecurityException, PrivateKey, PublicKey}

import org.apache.commons.codec.binary.Base64

import com.redmadrobot.websocket.common.Crypto
import com.redmadrobot.websocket.jwt.JwtHeaderEntryProtocol._
import com.redmadrobot.websocket.jwt.JwtPayloadEntryProtocol._
import com.typesafe.scalalogging._
import org.slf4j.LoggerFactory
import spray.json._

/**
  * JWT Helper object.
  *
  */
object JwtManager {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  private var privateKey: Option[PrivateKey] = None
  private var publicKey: Option[PublicKey] = None

  /**
    * Initialize jwt object and read public/private key files.
    *
    * @throws java.lang.Exception if error occurred
    */
  @throws(classOf[Exception])
  def init(): Unit = {
    val pvtBis = new BufferedInputStream(new FileInputStream("jwt/private_key.der"))
    val pvtBytes = Stream.continually(pvtBis.read).takeWhile(-1 !=).map(_.toByte).toArray
    val pubBis = new BufferedInputStream(new FileInputStream("jwt/public_key.der"))
    val pubBytes = Stream.continually(pubBis.read).takeWhile(-1 !=).map(_.toByte).toArray

    privateKey = Some(Crypto.readPrivateKey(pvtBytes))
    publicKey = Some(Crypto.readPublicKey(pubBytes))

    Crypto.init(privateKey.get, publicKey.get)
  }

  /**
    * Parse encoded Jwt String (including dot delimeter character) and return resulting entry as [[JwtEntry]].
    *
    * @param jwt jwt-token
    * @throws IllegalArgumentException if provided jwt is illegal
    * @throws FileNotFoundException if public key file wasn't found
    * @throws GeneralSecurityException on decryption error
    * @return jwt entry
    */
  @throws(classOf[GeneralSecurityException])
  def parseJwt(jwt: String): JwtEntry = {
    if (jwt == null || jwt.isEmpty) throw new IllegalArgumentException(jwt)

    if (publicKey.isEmpty) throw new IllegalStateException("Failed to process public key file")

    // Substring 'Bearer: '
    // eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJhMDAwMDAwMC0wMDAwLTQwMDAtYTAwMC0wMDAwMDAwMDAwMDAifQ.LHZvgD_7BqsyyD1w5oYkC9FqYdXcCJUjJkZScBAkz80UViAPeAzv_FaMDM83ZOL4-yZ7H721vWOJHk_zCHudNIXGOSY_3asril9QNSgbVlNaBYUzavEseJQnsNDZsJ8Zi4M9vw595tFdRntnFx-rq4LxrjzUE65iPYidxQnEN5u9cj33ho8Vz2yerMD1ggjd4YSy7dxGQpSZTvTPPba2zS25zPccvgGX2dopjJPCgFq2i-a8kS_jon2fEn2Xmj-bMF2Okmt5uPO_K351UL9lIxrdQC5hB7sLcT5c5raE4i1VcXrtQzRd-u-XEz9sVzmeTipnODAOIIlDi4PrYfkAQw

    var header = ""
    var payload = ""
    var signature = ""

    try {
      header = jwt.substring(0, jwt.indexOf('.'))
      payload = jwt.substring(jwt.indexOf('.') + 1, jwt.lastIndexOf('.'))
      signature = jwt.substring(jwt.lastIndexOf('.') + 1)
    } catch {
      case e: IndexOutOfBoundsException =>
        LOGGER.error(s"Failed to parse jwt token: $jwt\n${e.getMessage}")

        throw e
    }

    if (header.isEmpty) {
      throw new IllegalArgumentException("Jwt header is not provided")
    }

    if (payload.isEmpty) {
      throw new IllegalArgumentException("Jwt payload is not provided")
    }

    if (signature.isEmpty) {
      throw new IllegalArgumentException("Jwt signature is not provided")
    }

    val base64 = new Base64(true)

    val headerStr = header
    val headerEntry = new String(base64.decode(headerStr)).parseJson.convertTo[JwtHeaderEntry]

    if (headerEntry.alg != "RS256") {
      throw new IllegalArgumentException(s"Unsupported alg provided: ${headerEntry.alg}")
    }

    val payloadStr = payload
    LOGGER.debug(s"Signature string: $signature")
    val signatureDec = base64.decode(signature)

    LOGGER.debug(s"Encrypted signature: ${new String(signatureDec)}")

    try {
      Crypto.synchronized {
        val signature = Crypto.decrypt(signatureDec, publicKey.get)
        JwtEntry(headerEntry, Some(new String(base64.decode(payloadStr)).parseJson.convertTo[JwtPayloadEntry]), new String(signature))
      }
    } catch {
      case e: GeneralSecurityException =>
        throw e
    }
  }

  /**
    * Generate encoded jwt token.
    *
    * @param jwtHeaderEntry jwt header
    * @param jwtPayloadEntry jwt payload
    */
  @throws(classOf[Exception])
  def generateJwtToken(jwtHeaderEntry: JwtHeaderEntry, jwtPayloadEntry: JwtPayloadEntry): String = {
    if (privateKey.isEmpty) throw new FileNotFoundException("Failed to open private key contents")

    val headerStr = jwtHeaderEntry.toJson.toString()
    val payloadStr = jwtPayloadEntry.toJson.toString()

    val encHeaderStr = encodeJwtToBase64(headerStr.getBytes)
    val encPayloadStr = encodeJwtToBase64(payloadStr.getBytes)

    val signatureStr = s"$encHeaderStr.$encPayloadStr"
    LOGGER.debug(s"Signature size: ${signatureStr.getBytes.length}")

    val signatureRSA = Crypto.encrypt(signatureStr.getBytes, privateKey.get)
    LOGGER.debug(s"Encrypted signature: $signatureRSA")
    LOGGER.debug(s"Encrypted signature size: ${signatureRSA.length}")

    val encSignatureStr = encodeJwtToBase64(signatureRSA)
    s"$encHeaderStr.$encPayloadStr.$encSignatureStr"
  }



  /**
    * Encode Jwt bytes to Base64Url.
    *
    * @param jwt jwt bytes
    * @return result string
    */
  private def encodeJwtToBase64(jwt: Array[Byte]): String = {
    new Base64(true).encodeAsString(jwt).replaceAll("\r\n", "").replaceAll(" ", "")
  }
}
