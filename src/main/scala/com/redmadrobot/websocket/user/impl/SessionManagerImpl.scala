/*
 * [SessionManagerImpl.scala]
 * [wsproxy]
 *
 * Created by [Denis Pavlyuchenko] on 11 December 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.user.impl

import java.time.Duration
import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

import com.redmadrobot.websocket.logging.LoggingHelpers
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.backend.Task
import net.arammeem.chatprotobuf.messages.client.Response
import org.apache.http.client.methods.{CloseableHttpResponse, HttpGet}
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import redis.clients.jedis.{Jedis, JedisPool}
import scalapb.json4s.JsonFormat

import scala.collection.JavaConverters._
import scala.collection.mutable


/**
  * Management of user sessions
  */
class SessionManagerImpl(onlineUsersCacheLifetimeInSeconds: Int, jedisPool: JedisPool, closeableHttpClient: CloseableHttpClient,
                         chatBackendHost: String, chatBackendPort: Int) {

  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private final val USER_CACHE_NAME = "onlineusers"
  private final val REMOVE_NON_EXISTENT_USERS_PERIOD_IN_SECONDS = 30

  private val sessions = scala.collection.mutable.HashSet[String]()

  // Update last activity time for current online users at current application instance
  new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(() => {
    var jedis : Jedis = null

    try {
      jedis = jedisPool.getResource
      var sessionsClone : scala.collection.mutable.HashSet[String] = null
      sessions.synchronized {
        sessionsClone = sessions.clone()
      }

      if (sessionsClone.nonEmpty) {

        val scoreMembers = scala.collection.mutable.HashMap[java.lang.String, java.lang.Double]()
        val currentTimestamp = java.lang.Double.valueOf(System.currentTimeMillis().toString)
        for (userId <- sessionsClone) {
          scoreMembers.put(userId, currentTimestamp)
        }

        jedis.zadd(USER_CACHE_NAME, scoreMembers.asJava)
      }
    } catch {
      case e: Exception => LOGGER.error(s"Jedis error has occurred: ${e.getMessage}")
    } finally {
      if (jedis != null) {
        jedis.close()
      }
    }
  }, 1, onlineUsersCacheLifetimeInSeconds / 2, TimeUnit.SECONDS)

  // Remove evicted users from cache
  new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(() => {
    var jedis : Jedis = null

    try {
      jedis = jedisPool.getResource

      val maxEvictedTimestamp = java.lang.Double.valueOf((System.currentTimeMillis() - Duration.ofSeconds(onlineUsersCacheLifetimeInSeconds).toMillis).toString)
      jedis.zremrangeByScore(USER_CACHE_NAME, 0, maxEvictedTimestamp)

    } catch {
      case e: Exception => LOGGER.error(s"Jedis error has occurred: ${e.getMessage}")
    } finally {
      if (jedis != null) {
        jedis.close()
      }
    }
  }, 1, REMOVE_NON_EXISTENT_USERS_PERIOD_IN_SECONDS, TimeUnit.SECONDS)

  /**
    * Register session.
    *
    * @param userId user UUID
    */
  def registerSession(userId: String): Unit = {
    LOGGER.debug(s"Registering session for: $userId")

    var jedis : Jedis = null

    try {
      jedis = jedisPool.getResource
      jedis.zadd(USER_CACHE_NAME, System.currentTimeMillis(), userId)
    } catch {
      case e: Exception => LOGGER.error(s"Jedis error has occurred: ${e.getMessage}")
    } finally {
      if (jedis != null) {
        jedis.close()
      }
    }

    sessions.synchronized {
      sessions += userId
    }
  }

  /**
    * Remove session.
    *
    * @param userId user UUID
    */
  def removeSession(userId: String): Unit = {
    LOGGER.debug(s"Removing session for: $userId")

    var jedis : Jedis = null

    try {
      jedis = jedisPool.getResource
      jedis.zrem(USER_CACHE_NAME, userId)
    } catch {
      case e: Exception => LOGGER.error(s"Jedis error has occurred: ${e.getMessage}")
    } finally {
      if (jedis != null) {
        jedis.close()
      }
    }

    sessions.synchronized {
      sessions -= userId
    }
  }

  /**
    * Returns set of online users, who is belongs to given conversation.
    *
    * @param conversationId id of needed conversation
    * @return set of online users for given conversation
    */
  def getOnlineUsersInConversation(conversationId: String): mutable.Set[String] = {

    val onlineUserIdsInConversation = scala.collection.mutable.HashSet[String]()

    val response : CloseableHttpResponse = null
    var jedis : Jedis = null

    try {
      val response = closeableHttpClient.execute(new HttpGet(s"http://$chatBackendHost:$chatBackendPort/conversation/info/$conversationId"))
      val responseString = EntityUtils.toString(response.getEntity, "UTF-8")

      val task: Task = JsonFormat.fromJsonString[Task](responseString)

      if (task.getResponse.`type`.equals(Response.Type.ERROR)) {
        LOGGER.error(s"Cannot get conversation info: $responseString")
        return onlineUserIdsInConversation
      }

      val conversationOpt = task.getResponse.getConversations.conversationAndMessages
        .flatMap(c => c.conversation)
        .find(c => c.id.equals(conversationId))

      if (conversationOpt.isEmpty) {
        LOGGER.error(s"Conversation is empty: conversationId=: $conversationId")
        return onlineUserIdsInConversation
      }

      jedis = jedisPool.getResource
      val onlineUserIds = jedis.zrange(USER_CACHE_NAME, 0, -1)

      for (conversationUser <- conversationOpt.get.users) {
        if (onlineUserIds.contains(conversationUser.id)) {
          onlineUserIdsInConversation += conversationUser.id
        }
      }
    } catch {
      case e: Exception => LOGGER.error(s"Jedis error has occurred: ${e.getMessage}")
    } finally {
      if (response != null) {
        response.close()
      }
      if (jedis != null) {
        jedis.close()
      }
    }

    onlineUserIdsInConversation
  }
}
