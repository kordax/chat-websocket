/*
 * [SessionManager.scala]
 * [wsproxy]
 *
 * Created by [Denis Pavlyuchenko] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.user

import com.redmadrobot.websocket.user.impl.SessionManagerImpl
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory


object SessionManager {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private[this] var _sessionManager: SessionManagerImpl = _

  private def sessionManager_=(value: SessionManagerImpl): Unit = {
    _sessionManager = value
  }

  /**
    * Register sessionManager implementation.
    *
    * @param sessionManager [[SessionManagerImpl]] instance
    */
  def registerSessionManager(sessionManager: SessionManagerImpl): Unit = {
    LOGGER.debug("Registering session manager: " + sessionManager.toString)
    this._sessionManager = sessionManager
  }

  /**
    * Get valid sessionManager instance.
    *
    * @return instance of [[SessionManagerImpl]]
    */
  def sessionManager: SessionManagerImpl = {
    LOGGER.debug("Retrieving registered session manager")
    _sessionManager
  }
}
