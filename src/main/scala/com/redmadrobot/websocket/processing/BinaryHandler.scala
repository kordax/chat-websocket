/*
 * [BinaryHandler.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.processing

import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.BinaryMessage

/**
  * Message handler trait.
  *
  */
trait BinaryHandler {
  /**
    * Main handler method.
    *
    * @param userId user id
    * @param userTopic user topic for micro optimization
    * @param message message
    * @param actorRef actor reference
    * @throws java.lang.Exception if any error occurred
    */
  @throws(classOf[RuntimeException])
  def handle(userId: String, userTopic: String, message: BinaryMessage, actorRef: ActorRef): Unit
}
