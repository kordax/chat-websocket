/*
 * [HandlerManager.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.processing

import com.redmadrobot.websocket.processing.impl.KafkaHandlerImpl
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer

object HandlerManager {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private[this] var _handler: KafkaHandlerImpl = _

  private def handler_=(value: KafkaHandlerImpl): Unit = {
    _handler = value
  }

  /**
    * Registers [[KafkaHandlerImpl]] for [[akka.http.scaladsl.model.ws.BinaryMessage]] handling
    *
    * @param handler [[KafkaHandlerImpl]] instance
    */
  def registerHandler(handler: KafkaHandlerImpl): Unit = {
    LOGGER.debug("Registering binary handler: " + handler.toString)
    this._handler = handler
  }

  /**
    * Get valid handler
    *
    * @return [[ArrayBuffer]] of [[KafkaHandlerImpl]] instances
    */
  def handler: KafkaHandlerImpl = {
    LOGGER.debug("Retrieving registered binary handler")
    _handler
  }
}
