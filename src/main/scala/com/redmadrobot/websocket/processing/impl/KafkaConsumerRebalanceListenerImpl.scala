/*
 * [KafkaConsumerRebalanceListenerImpl.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 26 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.processing.impl

import java.util

import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.backend.Task
import org.apache.kafka.clients.consumer.{ConsumerRebalanceListener, KafkaConsumer}
import org.apache.kafka.common.TopicPartition
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Kafka consumer rebalance listener implementation.
  * This listener starts consumerThread if it's not started.
  *
  * @param consumer consumer reference
  * @param topic topic
  * @param onAssigned callback to call when partition was assigned
  * @param onRevoked callback to call when partition was revoked
  */
class KafkaConsumerRebalanceListenerImpl(val consumer: KafkaConsumer[String, Task],
                                         val topic: String,
                                         val onAssigned: TopicPartition => Unit,
                                         val onRevoked: TopicPartition => Unit) extends ConsumerRebalanceListener {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  override def onPartitionsAssigned(partitions: util.Collection[TopicPartition]): Unit = {
    LOGGER.debug(s"Assigned partitions: ${util.Arrays.toString(partitions.toArray())}")
    if (partitions.size() > 0) {
      val topicParts = consumer.partitionsFor(topic)
      val partition = new TopicPartition(topic, topicParts.get(0).partition())

      if (partitions.contains(partition)) {
        onAssigned(partition)
      }
    }
  }

  override def onPartitionsRevoked(partitions: util.Collection[TopicPartition]): Unit = {
    LOGGER.debug(s"Revoked partitions: ${util.Arrays.toString(partitions.toArray())}")

    partitions.forEach(partition =>
      onRevoked(partition)
    )
  }
}
