/*
 * [KafkaHandlerImpl.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.processing.impl

import java.util.concurrent._

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import com.redmadrobot.websocket.adapters.kafka.KafkaAdapter
import com.redmadrobot.websocket.config.Config
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.redmadrobot.websocket.processing.{BinaryHandler, ConsumerTask}
import com.redmadrobot.websocket.user.SessionManager
import com.redmadrobot.websocket.validation.RequestValidator
import com.redmadrobot.websocket.validation.exception.EmptyRequestIdException
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.backend.Task
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.OFFSET_OUTDATED
import net.arammeem.chatprotobuf.messages.client.Response.{Error, TypingActivity}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.TopicPartition
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Main [[akka.http.scaladsl.model.ws.BinaryMessage]] handler.
  * When new instance is created, provided MQ adapter will initiate connection.
  */
class KafkaHandlerImpl(adapter: KafkaAdapter[String], final val config: Config) extends BinaryHandler {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private val sessions = new scala.collection.mutable.HashMap[String, mutable.Set[ActorRef]] with mutable.MultiMap[String, ActorRef]
  private val sessionConsumers = new scala.collection.parallel.mutable.ParHashMap[ActorRef, KafkaConsumer[String, Task]]
  private val producer: KafkaProducer[String, Task] = adapter.getProducer
  private val validator: RequestValidator = new RequestValidator
  private val executor: ExecutorService = Executors.newWorkStealingPool(config.parallelism)
  private implicit val system: ActorSystem = ActorSystem()
  private implicit val materializer: Materializer = ActorMaterializer()

  private final val partition = config.partition
  private final val historyBound = config.historyBound

  sessionConsumers.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(2))

  private final val mainLoopThread = new Thread {
    private var shutdown = false

    def poisonPill(): Unit = {
      shutdown = true
    }

    override def run() {
      while(!shutdown) {
        try {
          sessionConsumers.foreach({ consumerSession =>
            executor.submit(new ConsumerTask(consumerSession._1, consumerSession._2, 1))
          })

          Thread.sleep(config.lingerMs)
        } catch {
          case _: InterruptedException =>
            LOGGER.warn("Interrupted main loop thread")
            Thread.currentThread().interrupt()
        }
      }
    }
  }

  mainLoopThread.start()

  /**
    * Register upgraded session.
    *
    * @param topic user topic
    * @param actorRef actor ref
    */
  def registerSession(topic: String, actorRef: ActorRef): Unit = {
    LOGGER.debug(s"Registering session for: $topic")
    val consumer = adapter.makeConsumer()
    consumer.assign(java.util.Arrays.asList(new TopicPartition(topic, partition)))
    sessions.synchronized {
      executor.submit(new ConsumerTask(actorRef, consumer, 1))
      sessions.addBinding(topic, actorRef)
      sessionConsumers.put(actorRef, consumer)
    }
  }

  /**
    * Register upgraded session.
    *
    * @param userId user UUID
    * @param actorRef actor ref
    */
  def removeSession(userId: String, actorRef: ActorRef): Unit = {
    LOGGER.debug(s"Removing session for: $userId, actor ${actorRef.toString()}")
    sessions.synchronized {
      sessionConsumers.remove(actorRef)
      sessions.removeBinding(userId, actorRef)
    }
  }

  /**
    * Main handler method.
    *
    * @param userId user id
    * @param userTopic user topic for micro optimization
    * @param message message
    * @param actorRef actor reference
    * @throws java.lang.Exception if any error occurred
    */
  @throws(classOf[RuntimeException])
  def handle(userId: String, userTopic: String, message: BinaryMessage, actorRef: ActorRef): Unit = {
    var id = ""
    var currentConsumer: KafkaConsumer[String, Task] = null

    sessions.synchronized {
      if (sessionConsumers.contains(actorRef)) {
        currentConsumer = sessionConsumers(actorRef)
      } else {
        return
      }
    }

    try {
      var request = Request()

      if (message.isStrict) {
        request = Request.parseFrom(message.getStrictData.toArray)
      } else {
        try {
          val data: concurrent.Future[Array[Byte]] = message.toStrict(10 seconds) map { strictMessage => strictMessage.getStrictData.toArray }
          val futResult: Array[Byte] = Await.result(data, 10 seconds)

          request = Request.parseFrom(futResult)
        } catch {
          case e: Exception =>
            LOGGER.error(s"Failed to retrieve streamed message: ${e.getMessage}, timeout: 10 seconds")

            actorRef ! BinaryMessage(ByteString(Response(Response.Type.ERROR, id, currentConsumer.synchronized { currentConsumer.position(new TopicPartition(userTopic, partition)) })
              .withError(
                Error(Error.Code.PRELIMINARY_MESSAGE_PARSE_FAILED, request.`type`)
              ).toByteArray))

            return
        }
      }

      id = request.id
      LOGGER.info(s"Received ${request.`type`}:\nSender id: $userId\nRequest id: ${request.id}")

      LOGGER.whenDebugEnabled {
        LOGGER.debug(s"Received request: ${request.toProtoString}")
      }

      try {
        validator.validate(request, () => currentConsumer.synchronized { currentConsumer.position(new TopicPartition(userTopic, partition)) })
      } catch {
        case e: EmptyRequestIdException =>
          LOGGER.error(e.getMessage)
          actorRef ! BinaryMessage(ByteString(Response(Response.Type.ERROR, id, currentConsumer.position(new TopicPartition(userTopic, partition)))
            .withError(
              Error(Error.Code.EMPTY_REQUEST_ID, request.`type`)
            ).toByteArray))

          return
      }

      if (id.isEmpty) {
        LOGGER.error("Request id is empty")
        actorRef ! BinaryMessage(ByteString(Response(Response.Type.ERROR, id, currentConsumer.synchronized { currentConsumer.position(new TopicPartition(userTopic, partition)) })
          .withError(
            Error(Error.Code.EMPTY_REQUEST_ID, request.`type`)
        ).toByteArray))

        return
      }

      // Handle LAST_OFFSET request
      if (request.`type`.isLastOffset) {
        currentConsumer.synchronized {
          if (!currentConsumer.assignment().contains(new TopicPartition(userTopic, partition))) {
            LOGGER.info(s"Assigning partition $userTopic:$partition")
            val partList = java.util.Arrays.asList(new TopicPartition(userTopic, partition))
            currentConsumer.assign(partList)
          }

          val response = Response(Response.Type.LAST_OFFSET, request.id, lastOffset(new TopicPartition(userTopic, partition), currentConsumer))
          LOGGER.info(s"Sending LAST_OFFSET response to request id: ${request.id}, offset: ${response.offset}")
          actorRef ! BinaryMessage(ByteString(response.toByteArray))

          return
        }
      }

      // Handle EVENT_LOG request, set offset to provided one
      if (request.`type`.isEventLog) {
        currentConsumer.synchronized {
          if (!currentConsumer.assignment().contains(new TopicPartition(userTopic, partition))) {
            LOGGER.whenDebugEnabled {
              LOGGER.debug(s"Assigning partition $userTopic:$partition with offset ${request.getEventLog.startOffset}")
            }

            val partList = java.util.Arrays.asList(new TopicPartition(userTopic, partition))
            currentConsumer.assign(partList)
            currentConsumer.seek(new TopicPartition(userTopic, partition), request.getEventLog.startOffset)
          } else {
            currentConsumer.seek(new TopicPartition(userTopic, partition), request.getEventLog.startOffset)
          }

          val loff = lastOffset(new TopicPartition(userTopic, partition), currentConsumer)

          if ((loff - request.getEventLog.startOffset) > historyBound) {
            val response = Response(Response.Type.ERROR, request.id, loff).withError(Error(OFFSET_OUTDATED, Request.Type.EVENT_LOG))
            LOGGER.whenDebugEnabled {
              LOGGER.debug(s"Sending OFFSET_OUTDATED response to request id: ${request.id}, offset: ${response.offset}")
            }
            actorRef ! BinaryMessage(ByteString(response.toByteArray))
          } else {
            val response = Response(Response.Type.EVENT_LOG, request.id, loff)
            LOGGER.whenDebugEnabled {
              LOGGER.debug(s"Sending EVENT_LOG response to request id: ${request.id}, offset: ${response.offset}")
            }
            actorRef ! BinaryMessage(ByteString(response.toByteArray))
          }

          return
        }
      }

      // Handle TYPING_ACTIVITY request
      if (request.`type`.isTypingActivity) {
        LOGGER.info(s"Creating typing activity task for request id :${request.id}")

        val onlineUserIds = SessionManager.sessionManager.getOnlineUsersInConversation(request.getTypingActivity.conversationId)
        for (onlineUserId <- onlineUserIds) {
          if (!onlineUserId.equals(userId)) {
            producer.send(new ProducerRecord[String, Task](
              KafkaAdapter.KAFKA_TOPIC_PREFIX_USER + onlineUserId,
              new Task(Task.Type.RESPONSE)
                .withSenderId(onlineUserId)
                .withResponse(Response(Response.Type.TYPING_ACTIVITY, request.id)
                  .withTypingActivity(
                    TypingActivity(request.getTypingActivity.conversationId, mapTypingActivityType(request.getTypingActivity.`type`), userId)
                  )
                ))
            )
          }
        }

        return
      }

      currentConsumer.synchronized {
        val part = new TopicPartition(userTopic, partition)
        if (!currentConsumer.assignment().contains(part)) {
          LOGGER.whenDebugEnabled {
            LOGGER.debug(s"Assigning partition $userTopic:$partition")
          }

          val partList = java.util.Arrays.asList(part)
          currentConsumer.assign(partList)
          currentConsumer.seekToEnd(partList)
          currentConsumer.position(part)
        }
      }

      // Else proceed to main logic
      try {
        LOGGER.info(s"Creating task for request id :${request.id}")
        producer.send(new ProducerRecord[String, Task](
          KafkaAdapter.KAFKA_TOPIC_INC_TASKS,
          new Task(Task.Type.REQUEST).withSenderId(userId).withRequest(request))
        )
      } catch {
        case default: Exception =>
          LOGGER.error(s"Failed to publish message: ${LoggingHelpers.getStackTraceAsString(default)}")

          currentConsumer.synchronized {
            val errMsg = Response(Response.Type.ERROR, request.id, currentConsumer.position(new TopicPartition(userTopic, partition)))
              .withError(Error(Error.Code.UNKNOWN_ERROR)
                .withRequestType(request.`type`)
              )
            actorRef ! BinaryMessage(ByteString(errMsg.toByteArray))
          }
      }
    } catch {
      case default: Exception =>
        currentConsumer.synchronized {
          LOGGER.error(s"Failed to parse message: ${LoggingHelpers.getStackTraceAsString(default)}")
          val errMsg = Response(Response.Type.ERROR, "", currentConsumer.position(new TopicPartition(userTopic, partition)))
            .withError(Error(Error.Code.PRELIMINARY_MESSAGE_PARSE_FAILED))

          actorRef ! BinaryMessage(ByteString(errMsg.toByteArray))
        }
    }
  }

  /**
    * Get latest kafka offset.
    * NOTE: This method should be synchronized upon consumer.
    *
    * @param topicPartition partition
    * @return offset
    */
  private def lastOffset(topicPartition: TopicPartition, consumer: KafkaConsumer[String, Task]): Long = {
    var result = -1L

    val prevPos = consumer.position(topicPartition)
    consumer.seekToEnd(java.util.Arrays.asList(topicPartition))
    result = consumer.position(topicPartition)
    consumer.seek(topicPartition, prevPos)

    result
  }

  private def mapTypingActivityType(requestTypingActivityType: Request.TypingActivity.Type) : Response.TypingActivity.Type = {
    if (requestTypingActivityType.isStart) {
      Response.TypingActivity.Type.START
    } else if (requestTypingActivityType.isStop) {
      Response.TypingActivity.Type.STOP
    } else {
      LOGGER.error(s"cannot map typing activity type: ${requestTypingActivityType.toString()}")
      throw new IllegalArgumentException("cannot map typing activity type: " + requestTypingActivityType.toString())
    }
  }
}
