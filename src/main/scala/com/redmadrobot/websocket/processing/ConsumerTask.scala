/*
 * [Consumertask.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 02 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.processing

import java.time.Duration

import akka.actor.ActorRef
import akka.util.ByteString
import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.backend.Task
import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.errors.TimeoutException
import org.slf4j.LoggerFactory

/**
  * Kafka consumer task.
  *
  * @param session current session reference as [[ActorRef]]
  * @param consumer provided consumer
  * @param partition current partition
  */
class ConsumerTask(final val session: ActorRef, final val consumer: KafkaConsumer[String, Task], final val partition: Int) extends Runnable {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private var consIt: java.util.Iterator[ConsumerRecord[String, Task]] = _

  override def run(): Unit = {
    try {
      var offset = 0L

      consumer.synchronized {
        if (consumer.assignment().isEmpty) {
          return
        }
        
        consIt = consumer.poll(Duration.ZERO).iterator()
      }

      while (consIt.hasNext) {
        val record = consIt.next
        offset = record.offset()

        LOGGER.whenDebugEnabled {
          LOGGER.debug(s"[${record.topic}] Consumed task:\n${record.value.toProtoString}")

          if (record.value.getResponse.`type`.isEvent) {
            LOGGER.debug(s"Sending ${record.value.getResponse.getEvent.`type`.toString} with request id: ${record.value.getResponse.id}, offset $offset")
          } else {
            LOGGER.debug(s"Sending ${record.value.getResponse.`type`.toString} with request id: ${record.value.getResponse.id}, offset $offset")
          }
        }

        session ! akka.http.scaladsl.model.ws.BinaryMessage(ByteString(record.value.getResponse.withOffset(offset).toByteArray))
      }

      consumer.synchronized {
        consumer.commitSync(Duration.ofMillis(3000))
      }
    } catch {
      case _t: TimeoutException =>
        // We don't really care as we resend such tasks again and again
        LOGGER.warn("Timeout has occurred while trying to commit offset")
      case _: InterruptedException =>
        LOGGER.warn("Interrupted main loop thread")
        Thread.currentThread().interrupt()
      case e: Exception => LOGGER.error(s"Consumer task unknown exception", e)
    }
  }
}