/*
 * [RequestValidatorBasicImpl.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.validation

import com.typesafe.scalalogging.Logger
import net.arammeem.chatprotobuf.messages.client.Request.EventLog
import net.arammeem.chatprotobuf.messages.client.Response.Error.Code.{OFFSET_OUTDATED, VALIDATION_ERROR}
import net.arammeem.chatprotobuf.messages.client.Response.{Error, ValidationError}
import net.arammeem.chatprotobuf.messages.client.{Request, Response}
import org.slf4j.LoggerFactory
import scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}

/**
  * Request validator implementation.
  * This implementation provides only basic checks to avoid influence on performance.
  *
  */
class RequestValidator {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  /**
    * Validate request.
    *
    * @param request request
    * @return [[Option]] with [[Response]] if error has occurred, [[None]] otherwise
    */
  def validate(request: Request, retrieveLastOffset: () => Long): Option[Response] = {
    LOGGER.debug(s"Validating request: ${request.id}")
    if(request.id.isEmpty) {
      return Some(Response(Response.Type.ERROR, request.id, retrieveLastOffset())
        .withError(Error(OFFSET_OUTDATED, Request.Type.EVENT_LOG)))
    }

    if (request.taskMessageOneOf.isEmpty) {
      if(!request.`type`.isLastOffset) {
        return Some(Response(Response.Type.ERROR, request.id, retrieveLastOffset())
          .withError(
            Error(VALIDATION_ERROR, Request.Type.EVENT_LOG)
              .withValidationError(
                ValidationError("oneOf", ValidationError.Code.UNDEFINED_PARAMETER)
              )
          )
        )
      }
    }

    if (request.`type`.isEventLog) {
      if (request.getEventLog.startOffset < 0L) {
        return Some(Response(Response.Type.ERROR, request.id, retrieveLastOffset())
          .withError(
            Error(VALIDATION_ERROR, Request.Type.EVENT_LOG)
              .withValidationError(
                ValidationError(getFieldName[EventLog](request.getEventLog.companion.messageCompanion, EventLog.STARTOFFSET_FIELD_NUMBER), ValidationError.Code.VALUE_IS_BELOW_MINIMUM_VALUE)
              )
          )
        )
      }
    }

    None
  }


  /**
    * Retrieve field name from message type descriptor.
    *
    * @param messageCompanion protobuf message companion object [[GeneratedMessageCompanion]]
    * @param fieldNumber field number
    * @return field name or 'unknown field' if field wasn't found by field number for current message structure
    */
  private def getFieldName[T <: GeneratedMessage with Message[T]](messageCompanion: GeneratedMessageCompanion[T], fieldNumber: Int): String = {
    val field = messageCompanion.scalaDescriptor.findFieldByNumber(fieldNumber)

    if (field.isDefined) {
      messageCompanion.scalaDescriptor.findFieldByNumber(fieldNumber).get.name
    } else {
      "unknown field"
    }
  }
}
