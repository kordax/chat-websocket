/*
 * [ValidationException.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.validation.exception

/**
  * Validation exception.
  *
  * @param message message
  * @param cause cause
  */
class ValidationException(private val message: String = "", private val cause: Throwable = None.orNull) extends Exception(message, cause)