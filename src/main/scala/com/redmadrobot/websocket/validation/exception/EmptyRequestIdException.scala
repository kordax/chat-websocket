/*
 * [NullIdException.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.validation.exception

/**
  * Empty request id exception.
  *
  * @param message message
  * @param cause cause
  */
final case class EmptyRequestIdException(private val message: String = "", private val cause: Throwable = None.orNull) extends ValidationException(message, cause)