/*
 * [Routes.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 25 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.routing

import java.security.GeneralSecurityException
import java.util.concurrent.{TimeUnit, TimeoutException}

import akka.actor.{ActorRef, ActorSystem, PoisonPill, Props}
import akka.event.Logging
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage, UpgradeToWebSocket}
import akka.stream.ActorMaterializer
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.{Done, NotUsed}
import com.redmadrobot.websocket.adapters.kafka.KafkaAdapter
import com.redmadrobot.websocket.common.Constants
import com.redmadrobot.websocket.jwt.{JwtEntry, JwtManager}
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.redmadrobot.websocket.processing.HandlerManager
import com.redmadrobot.websocket.user.SessionManager
import com.redmadrobot.websocket.util.VersionUtils
import com.typesafe.scalalogging.Logger
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.common.errors.InvalidReplicationFactorException
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * Basic object that contains implemented AKKA routes for HTTP server singleton.
  *
  */
class Routes(kafkaAdapter: KafkaAdapter[String]) {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  implicit val system: ActorSystem = ActorSystem(Logging.simpleName(this).replaceAll("\\$", ""))
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  private final val uriStr = s"/${Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX.toString}"
  private final val healthUriStr = s"/${Constants.WSPROXY_HEALTH_CHECK_PATH_SUFFIX.toString}"
  private final val routes = new java.util.concurrent.ConcurrentHashMap[ActorRef, String] // Map of Actor references and their user ids.
  private final val appApiVersion = VersionUtils.retrieveApiVersionString

  private val adminClient = kafkaAdapter.makeAdminClient()

  /**
    * Synchronous handler.
    */
  val syncHandler: HttpRequest => HttpResponse = {
    case _ @ HttpRequest(HttpMethods.GET, Uri.Path(`healthUriStr`), _, _, _) =>
      HttpResponse(200, entity = "Health check response")
    case req @ HttpRequest(HttpMethods.GET, Uri.Path(`uriStr`), _, _, _) =>
      LOGGER.whenDebugEnabled {
        LOGGER.debug(s"Provided headers:\n ${req.headers.toString()}")
      }
      req.header[UpgradeToWebSocket] match {
        case Some(upgrade) =>
          req.uri.query().get(Constants.API_VERSION_GET_PARAMETER_NAME) match {
            case Some(apiVersionParam) =>
              try {
                val applicationVersion = VersionUtils.getApiVersionFromString(appApiVersion)
                val clientApiVersion = VersionUtils.getApiVersionFromString(apiVersionParam)

                if (clientApiVersion.minor != applicationVersion.minor || applicationVersion.major != clientApiVersion.major) {
                  LOGGER.error(s"Api version mismatch: '${clientApiVersion.toString}', current version '${applicationVersion.toString}'")
                  req.discardEntityBytes()
                  HttpResponse(501)
                }

                req.uri.query().get(Constants.JWT_GET_PARAMETER_NAME) match {
                  case Some(jwtToken) =>
                    try {
                      val jwt: JwtEntry = JwtManager.parseJwt(jwtToken)

                      val routeActorRef = system.actorOf(Props[RouteActor])
                      val flow = wsRouteFlow(routeActorRef)

                      val userId = jwt.jwtPayloadEntry.get.userId

                      val topicName = KafkaAdapter.KAFKA_TOPIC_PREFIX_USER + userId

                      if (!adminClient.listTopics().names().get().stream().anyMatch(listedTopicName => listedTopicName.equals(topicName))) {
                        LOGGER.info(s"User topic doesn't exist, creating topic: $topicName")

                        try {
                          createUserTopic(topicName, 1)
                        } catch {
                          case _: TimeoutException =>
                            LOGGER.error(s"Timeout has occurred while trying to create user topic: $topicName")

                            req.discardEntityBytes()
                            HttpResponse(502)
                          case e: InvalidReplicationFactorException =>
                            LOGGER.error(s"Invalid replication factor provided while trying to create user topic [$topicName]: ${e.getMessage}")

                            req.discardEntityBytes()
                            HttpResponse(502)
                        }
                      }


                      routes.put(routeActorRef, s"$userId")
                      HandlerManager.handler.registerSession(topicName, routeActorRef)
                      SessionManager.sessionManager.registerSession(userId)
                      LOGGER.info(s"Authorized test client $userId, current number of clients: ${routes.size}")

                      upgrade.handleMessages(flow)
                    } catch {
                      case e: IllegalArgumentException =>
                        LOGGER.error(s"Failed to parse jwt token", e)
                        req.discardEntityBytes()
                        HttpResponse(401)
                      case sec: GeneralSecurityException =>
                        LOGGER.error(s"Failed to decrypt jwt signature: ${LoggingHelpers.getStackTraceAsString(sec)}")
                        req.discardEntityBytes()
                        HttpResponse(401)
                    }
                  case None =>
                    LOGGER.error(s"Missing ${Constants.JWT_GET_PARAMETER_NAME} GET-Parameter")
                    req.discardEntityBytes()
                    HttpResponse(401)
                }
              } catch {
                case iae: IllegalArgumentException =>
                  LOGGER.error(s"Failed to parse version string: $apiVersionParam", iae)
                  req.discardEntityBytes()
                  HttpResponse(422)
                case e: Exception =>
                  LOGGER.error(s"Unknown exception has occurred with version: $apiVersionParam", e)
                  req.discardEntityBytes()
                  HttpResponse(500)
              }
            case None =>
              LOGGER.error(s"Missing ${Constants.API_VERSION_GET_PARAMETER_NAME} GET-Parameter")
              req.discardEntityBytes()
              HttpResponse(400)
          }
        case None =>
          req.discardEntityBytes()
          HttpResponse(400)
      }
    case r: HttpRequest =>
      r.discardEntityBytes()
      HttpResponse(404, entity = "Unknown resource")
  }

  /**
    * Main asynchronous handler that's passed to AKKA Http() singleton instance.
    */
  val asyncHandler: HttpRequest => Future[HttpResponse] = req => Future.successful(syncHandler(req))

  /**
    * Flow method to call request handler on binary messages.
    *
    * @return flow for handleWebSocketMessages method
    */
  private def wsRouteFlow(routeActorRef: ActorRef): Flow[Message, Message, Any] = {
    val routeActor = ActorPublisher[Message](routeActorRef)

    val incomingMessages: Sink[Message, NotUsed] = Flow[Message].map {
      case binaryMessage: BinaryMessage =>
        try {
          val userId = routes.get(routeActorRef)
          LOGGER.debug(s"Received request from: $userId")
          HandlerManager.handler.handle(routes.get(routeActorRef), KafkaAdapter.KAFKA_TOPIC_PREFIX_USER + userId, binaryMessage, routeActorRef)
        } catch {
          case default: Exception =>
            LOGGER.error(LoggingHelpers.getStackTraceAsString(default))
        }
      case _: TextMessage =>
        TextMessage(s"Text messages are not supported!")
        routeActorRef ! PoisonPill
    }.to(Sink.actorRef(routeActorRef, PoisonPill))

    val outgoingMessages: Source[Message, Unit] = Source.fromPublisher(routeActor).watchTermination() { (_, future: Future[Done]) =>
      future onComplete {
        case Success(_) =>
          val userId = routes.get(routeActorRef)
          routes.remove(routeActorRef)
          HandlerManager.handler.removeSession(KafkaAdapter.KAFKA_TOPIC_PREFIX_USER + userId, routeActorRef)
          SessionManager.sessionManager.removeSession(userId)
          LOGGER.info(s"""Client "$userId" has disconnected, current number of clients ${routes.size}""")

          routeActorRef ! PoisonPill
        case Failure(ex) =>
          val userId = routes.get(routeActorRef)
          routes.remove(routeActorRef)
          HandlerManager.handler.removeSession(KafkaAdapter.KAFKA_TOPIC_PREFIX_USER + userId, routeActorRef)
          SessionManager.sessionManager.removeSession(userId)
          LOGGER.info(s"""Client "$userId" connection has been dropped with error: ${ex.getMessage}""")
          LOGGER.info(s"Current number of clients ${routes.size}")
          
          routeActorRef ! PoisonPill
      }
    }

    Flow.fromSinkAndSource(incomingMessages, outgoingMessages)
  }

  /**
    * Create user topic in recursive way while trying to decrease current available brokers number.
    *
    * @param topicName topic name
    * @param replicationFactor replication factor
    */
  @throws(classOf[TimeoutException])
  @throws(classOf[InvalidReplicationFactorException])
  private def createUserTopic(topicName: String, replicationFactor: Short): Unit = {
    val createTopicsResults = adminClient.createTopics(java.util.Arrays.asList[NewTopic](new NewTopic(topicName, 1, replicationFactor)))

    try {
      createTopicsResults.values().get(topicName).get(30, TimeUnit.SECONDS)
    } catch {
      case te: TimeoutException =>
        LOGGER.error(s"Timeout has occurred while trying to create user topic: $topicName")

        throw te
      case irfe: InvalidReplicationFactorException =>
        LOGGER.error(s"Invalid replication factor provided while trying to create user topic [$topicName]: ${irfe.getMessage}")

        throw irfe
    }
  }
}
