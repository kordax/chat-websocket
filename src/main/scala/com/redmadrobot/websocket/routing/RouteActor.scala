/*
 * [RouteActor.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 25 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.routing

import akka.actor.Props
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.Cancel

import scala.concurrent.ExecutionContext

class RouteActor extends ActorPublisher[BinaryMessage] {
  override def receive: Receive = {
    case msg: BinaryMessage =>
      while (totalDemand == 0 && isActive) {
        onCompleteThenStop()
      }

      if (totalDemand > 0) {
        onNext(msg)
      }
    case Cancel =>
      context.stop(self)
    case _ =>
  }
}

object RouteActor {
  def props(implicit ctx: ExecutionContext): Props = Props(new RouteActor())
}