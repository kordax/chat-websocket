/*
 * [WebSocketShutdownHook.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 25 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket

import ch.qos.logback.core.hook.ShutdownHookBase
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
  * WebSocket ShutdownHook via extension.
  */
class WebSocketShutdownHook() extends ShutdownHookBase {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  /**
    * Default method for stopping the Logback context and AKKA WebSocket binding.
    */
  override def stop(): Unit = {
    LOGGER.info("Shutting down WebSocket proxy")

    if (Main.shutdownHook.isDefined) {
      Main.shutdownHook.get.run()
    }

    super.stop()
  }

  override def run(): Unit = {
    stop()
  }
}