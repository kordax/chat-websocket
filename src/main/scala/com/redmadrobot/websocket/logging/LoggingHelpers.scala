/*
 * [LoggingHelpers.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.logging

import java.io.{PrintWriter, StringWriter}

/**
  *
  */
object LoggingHelpers {
  /**
    * Get exception stack trace as a [[String]]
    *
    * @param t throwable
    * @return stack trace as a [[String]]
    */
  def getStackTraceAsString(t: Throwable): String = {
    val sw = new StringWriter
    t.printStackTrace(new PrintWriter(sw))
    sw.toString
  }
}
