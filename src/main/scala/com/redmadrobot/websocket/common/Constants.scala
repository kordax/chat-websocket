/*
 * [Constants.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.common

object Constants {
  final val WSPROXY_WEBSOCKET_PATH_SUFFIX = "ws"
  final val WEB_SOCKET_NONSECURE_PROTOCOL = "ws"
  final val WEB_SOCKET_SECURE_PROTOCOL = "wss"
  final val WSPROXY_HEALTH_CHECK_PATH_SUFFIX = "health"
  final val JWT_GET_PARAMETER_NAME = "jwt"
  final val API_VERSION_GET_PARAMETER_NAME = "api"
}
