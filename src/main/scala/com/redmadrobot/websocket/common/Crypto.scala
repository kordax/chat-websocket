/*
 * [Crypto.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.common

import java.security._
import java.security.spec.{PKCS8EncodedKeySpec, X509EncodedKeySpec}

import com.typesafe.scalalogging.Logger
import javax.crypto.Cipher
import org.slf4j.LoggerFactory

/**
  * Crypto helper object that provides helper methods for crypto operations.
  *
  */
object Crypto {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private final val alg = "RSA"
  private final val encCipher = Cipher.getInstance(alg)
  private final val decCipher = Cipher.getInstance(alg)

  def init(privateKey: PrivateKey, publicKey: PublicKey): Unit = {
    decCipher.init(Cipher.DECRYPT_MODE, publicKey)
    encCipher.init(Cipher.ENCRYPT_MODE, privateKey)
  }

  /**
    * Encrypt data using private key.
    *
    * @param data data
    * @param privateKey private key
    * @throws Exception if error occurred
    * @return encrypted data as String
    */
  @throws[Exception]
  def encrypt(data: Array[Byte], privateKey: PrivateKey): Array[Byte] = {
    LOGGER.debug(s"Encrypting ${data.length} bytes")
    encCipher.doFinal(data)
  }

  /**
    * Decrypt data using public key.
    *
    * @param data data
    * @param publicKey public key
    * @throws Exception if error occurred
    * @return decrypted data as String
    */
  @throws[Exception]
  def decrypt(data: Array[Byte], publicKey: PublicKey): Array[Byte] = {
    LOGGER.debug(s"Decrypting ${data.length} bytes")
    decCipher.doFinal(data)
  }

  /**
    * Read public key from file source bytes.
    *
    * @param source source bytes
    * @param alg algorithm
    * @throws Exception if error occurred
    * @return
    */
  @throws[Exception]
  def readPublicKey(source: Array[Byte], alg: String = alg): PublicKey = {
    val keySpec = new X509EncodedKeySpec(source)
    val kf = KeyFactory.getInstance(alg)

    LOGGER.debug(s"Reading public key")

    kf.generatePublic(keySpec)
  }

  /**
    * Read private key from file source bytes.
    *
    * @param source source bytes
    * @param alg algorithm
    * @throws Exception if error occurred
    * @return
    */
  @throws[Exception]
  def readPrivateKey(source: Array[Byte], alg: String = alg): PrivateKey = {
    val keySpec = new PKCS8EncodedKeySpec(source)
    val kf = KeyFactory.getInstance(alg)

    LOGGER.debug(s"Reading private key")

    kf.generatePrivate(keySpec)
  }
}
