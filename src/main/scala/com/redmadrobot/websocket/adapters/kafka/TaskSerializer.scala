/*
 * [TaskSerializer.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.adapters.kafka

import java.util

import net.arammeem.chatprotobuf.messages.backend.Task
import com.typesafe.scalalogging.Logger
import org.apache.kafka.common.serialization.Serializer
import org.slf4j.LoggerFactory

/**
  * ProtoBuf messages deserializer for KafkaAdapter
  *
  */
class TaskSerializer extends Serializer[Task] {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  /**
    * Configure this class.
    *
    * @param configs configs in key/value pairs
    * @param isKey whether is for key or value
    */
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {
    LOGGER.debug("Configuring deserializer")
  }

  /**
    * Convert `data` into a byte array.
    *
    * @param topic topic associated with data
    * @param data typed data
    * @return serialized bytes
    */
  override def serialize(topic: String, data: Task): Array[Byte] = {
    LOGGER.debug(s"""Serializing message for topic "$topic"""")
    data.toByteArray
  }

  /**
    * Close this serializer.
    *
    * This method must be idempotent as it may be called multiple times.
    */
  override def close(): Unit = {
    // There's nothing to do on close for this serializer
  }
}