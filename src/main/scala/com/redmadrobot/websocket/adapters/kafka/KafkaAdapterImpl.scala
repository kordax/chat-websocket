/*
 * [KafkaKafkaAdapterImpl.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.adapters.kafka

import java.util.{Properties, UUID}

import net.arammeem.chatprotobuf.messages.backend.Task
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}

/**
  * Kafka producer and consumer adapter.
  *
  * @param bootstrapServers  bootstrap servers string
  * @param host              host
  * @param prodProperties    optional producer properties
  * @param consProperties    optional consumer properties
  */
class KafkaAdapterImpl(bootstrapServers: String,
                       host: String,
                       prodProperties: Properties = new Properties(),
                       consProperties: Properties = new Properties()
                      ) extends KafkaAdapter[String] {
  private val producerProperties: Properties = prodProperties
  private val consumerProperties: Properties = consProperties

  // Initialize block
  {
    producerProperties.put("bootstrap.servers", bootstrapServers)
    consumerProperties.put("bootstrap.servers", bootstrapServers)
    consumerProperties.put("group.id", s"chat-websocket-$host-${UUID.randomUUID().toString}")
    consumerProperties.put("client.id", s"$host-${UUID.randomUUID().toString}")
    consumerProperties.put("session.timeout.ms", "15000")
    consumerProperties.put("max.poll.records", "2000")
    consumerProperties.put("auto.offset.reset", "latest")
  }

  private val producer = new KafkaProducer[String, Task](producerProperties, new StringSerializer, new TaskSerializer)
  private val consumer = new KafkaConsumer[String, Task](consumerProperties, new StringDeserializer, new TaskDeserializer)

  /**
    * Get producer
    *
    * @return [[KafkaProducer]] instance
    */
  override def getProducer: KafkaProducer[String, Task] = {
    producer
  }

  /**
    * Get consumer
    *
    * @return [[KafkaConsumer]] instance
    */
  override def getConsumer: KafkaConsumer[String, Task] = {
    consumer
  }

  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  override def getConnectionString: String = {
    bootstrapServers
  }

  /**
    * Create new producer instance with same parameters
    *
    * @return new kafka producer
    */
  override def makeProducer(): KafkaProducer[String, Task] = {
    new KafkaProducer[String, Task](prodProperties, new StringSerializer, new TaskSerializer)
  }

  /**
    * Create new consumer instance with same parameters
    *
    * @return new kafka consumer
    */
  override def makeConsumer(): KafkaConsumer[String, Task] = {
    val propsCopy: Properties = new Properties()
    consumerProperties.forEach((k, v) => propsCopy.put(k, v))
    propsCopy.put("group.id", s"chat-websocket-$host-${UUID.randomUUID().toString}")
    propsCopy.put("client.id", s"$host-${UUID.randomUUID().toString}")
    new KafkaConsumer[String, Task](propsCopy, new StringDeserializer, new TaskDeserializer)
  }

  /**
    * Create new kafka admin client.
    *
    * @return new kafka admin client
    */
  override def makeAdminClient(): AdminClient = {
    import org.apache.kafka.clients.admin.AdminClient
    val properties = new Properties
    properties.setProperty("bootstrap.servers", bootstrapServers)
    properties.setProperty("client.id", s"kafka-admin-client-$host")

    AdminClient.create(properties)
  }
}