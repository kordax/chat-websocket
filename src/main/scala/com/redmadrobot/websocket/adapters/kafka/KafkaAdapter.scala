/*
 * [KafkaAdapter.scala]
 * [wsproxy]
 *
 * Created by [Dmitry Morozov] on 10 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket.adapters.kafka

import net.arammeem.chatprotobuf.messages.backend.Task
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer

/**
  * Kafka Adapter trait
  */
trait KafkaAdapter[Key] {
  /**
    * Get producer
    *
    * @return [[KafkaProducer]] instance
    */
  def getProducer: KafkaProducer[Key, Task]

  /**
    * Get consumer
    *
    * @return [[KafkaConsumer]] instance
    */
  def getConsumer: KafkaConsumer[Key, Task]

  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  def getConnectionString: String

  /**
    * Create new producer instance with same parameters
    *
    */
  def makeProducer(): KafkaProducer[Key, Task]

  /**
    * Create new consumer instance with same parameters
    *
    */
  def makeConsumer(): KafkaConsumer[Key, Task]

  /**
    * Create new kafka admin client.
    *
    * @return new kafka admin client
    */
  def makeAdminClient(): AdminClient
}

object KafkaAdapter {
  val KAFKA_TOPIC_INC_TASKS = "IncTasksTopic"
  val KAFKA_TOPIC_OUT_TASKS = "OutTasksTopic"
  val KAFKA_TOPIC_PREFIX_TRANSPORT = "TransportTopic"
  val KAFKA_TOPIC_PREFIX_USER = "UserTopic"
  val KAFKA_TOPIC_TASKS_TEST = "TestTasksTopic"
}