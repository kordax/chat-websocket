/*
 * [Main.scala]
 * [chat-websocket]
 *
 * Created by [Dmitry Morozov] on 25 October 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package com.redmadrobot.websocket

import java.net.InetAddress
import java.time.Duration
import java.util.regex.Pattern

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.redmadrobot.websocket.adapters.kafka.{KafkaAdapter, KafkaAdapterImpl}
import com.redmadrobot.websocket.common.Constants
import com.redmadrobot.websocket.config.{ArgsParser, Config}
import com.redmadrobot.websocket.jwt.JwtManager
import com.redmadrobot.websocket.logging.LoggingHelpers
import com.redmadrobot.websocket.processing.HandlerManager
import com.redmadrobot.websocket.processing.impl.KafkaHandlerImpl
import com.redmadrobot.websocket.routing.Routes
import com.redmadrobot.websocket.user.SessionManager
import com.redmadrobot.websocket.user.impl.SessionManagerImpl
import com.typesafe.scalalogging.Logger
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.{CloseableHttpClient, HttpClients}
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener
import org.apache.kafka.common.TopicPartition
import org.slf4j.LoggerFactory
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.sys.ShutdownHookThread

/**
  * Main application object.
  */
object Main extends App {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))

  implicit val _system: ActorSystem = ActorSystem("akka-system")
  implicit val _materializer: ActorMaterializer = ActorMaterializer()
  var _shutdownHook: Option[ShutdownHookThread] = None
  var _routes: Routes = _

  private def routes: Routes = _routes

  private def routes_=(value: Routes): Unit = {
    _routes = value
  }

  def shutdownHook: Option[ShutdownHookThread] = _shutdownHook

  def shutdownHook_=(value: Option[ShutdownHookThread]): Unit = {
    _shutdownHook = value
  }


  LOGGER.info("Starting WebSocket proxy")

  val config = parseArgs()
  initialize(30000)

  try {
    JwtManager.init()
  } catch {
    case e: Exception =>
      LOGGER.info("Failed to initialize JWT data: " + LoggingHelpers.getStackTraceAsString(e))

      System.exit(1);
  }

  val binding = Http().bindAndHandleAsync(routes.asyncHandler, config.host, config.port)

  _shutdownHook = Some(sys.addShutdownHook({
    if (binding.isCompleted) {
      binding.flatMap(_.unbind()).onComplete(_ => _system.terminate())
    }
  }))

  if (config.secure) {
    LOGGER.info(s"WebSocket proxy is now online at wss://${config.host}:${config.port}/${Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX}")
  } else {
    LOGGER.info(s"WebSocket proxy is now online at ws://${config.host}:${config.port}/${Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX}")
  }

  /**
    * Parse application arguments.
    */
  private def parseArgs(): Config = {
    LOGGER.info(s"Arguments size: ${args.length}")
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    LOGGER.info(s"Provided configuration: ${config.toString}")

    config
  }

  /**
    * Register routes and ProtoBuf Kafka handlers.
    *
    * @param timeoutMS timeout in milliseconds
    */
  private def initialize(timeoutMS: Int): Unit = {
    // Register Kafka handler
    val host = InetAddress.getLocalHost.getHostName

    val jedisPool = new JedisPool(new JedisPoolConfig, config.redisHost, config.redisPort, config.redisConnectionTimeout)
    val closeableHttpClient = buildHttpClient()
    SessionManager.registerSessionManager(new SessionManagerImpl(config.onlineUsersCacheLifetimeInSeconds, jedisPool,
      closeableHttpClient, config.redisHost, config.redisPort))

    val kafkaAdapter = new KafkaAdapterImpl(config.kafka, host)
    HandlerManager.registerHandler(new KafkaHandlerImpl(kafkaAdapter, config))
    routes = new Routes(kafkaAdapter)

    // Force subscription to IncTasksTopic and assign consumer group, and then unsubscribe from all topics
    var assigned = false
    kafkaAdapter.getConsumer.subscribe(Pattern.compile(KafkaAdapter.KAFKA_TOPIC_INC_TASKS), new ConsumerRebalanceListener() {
      override def onPartitionsRevoked(partitions: java.util.Collection[TopicPartition]): Unit = {

      }

      override def onPartitionsAssigned(partitions: java.util.Collection[TopicPartition]): Unit = {
        assigned = true
      }
    })

    while (!assigned) {kafkaAdapter.getConsumer.poll(Duration.ZERO); Thread.sleep(500)}
    kafkaAdapter.getConsumer.unsubscribe()
  }

  private def buildHttpClient(): CloseableHttpClient = {
    val cm = new PoolingHttpClientConnectionManager()
    cm.setMaxTotal(100)
    cm.setDefaultMaxPerRoute(20)

    val requestConfig = RequestConfig.custom()
      .setConnectTimeout(config.httpClientConnectionTimeout)
      .setSocketTimeout(config.httpClientConnectionTimeout)
      .build()

    HttpClients.custom()
      .setConnectionManager(cm)
      .setDefaultRequestConfig(requestConfig)
      .build()
  }
}
