package com.redmadrobot.websocket.util

/**
  * API/Application version entry
  *
  * @param major major number
  * @param minor minor number
  * @param sprint sprint number
  * @param build build number
  */
case class VersionEntry(major: Int, minor: Int, sprint: Int, build: Int)
