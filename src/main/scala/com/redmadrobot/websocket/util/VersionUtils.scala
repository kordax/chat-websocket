package com.redmadrobot.websocket.util

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.io.Source

/**
  * Version utils.
  *
  */
object VersionUtils {
  private final val LOGGER = Logger(LoggerFactory.getLogger(this.getClass))
  private var versionString: String = ""

  /**
    * Update current version string.
    *
    */
  def updateVersionString(): Unit = {
    versionString = retrieveApiVersionString
    LOGGER.info(s"Using API verison: $versionString")
  }

  /**
    * Retrieve version string.
    *
    */
  def getVersionString(): String = {
    versionString
  }

  /**
    * Retrieve API version string.
    *
    * @throws java.lang.NullPointerException if error occurred
    * @throws java.lang.IllegalArgumentException if version string is invalid
    * @return result
    */
  @throws(classOf[NullPointerException])
  @throws(classOf[IllegalArgumentException])
  def retrieveApiVersionString: String = {
    val versionLine = Source.fromFile("build.sbt").getLines.filter(line => line.contains("chat-protobuf_2.12")).next()
    val result = versionLine.substring("libraryDependencies += \"com.redmadrobot\" % \"chat-protobuf_2.12\" % \"".length, versionLine.length).replace("\"", "")

    if (result.isEmpty) {
      throw new IllegalArgumentException("Failed to retrieve valid version string")
    }

    result
  }

  /**
    * Parse version string as [[VersionEntry]].
    *
    * @param versionString version string
    * @throws java.lang.IllegalArgumentException if provided string is invalid
    * @return
    */
  @throws(classOf[IllegalArgumentException])
  @throws(classOf[NumberFormatException])
  def getApiVersionFromString(versionString: String): VersionEntry = {
    val numbers = versionString.split('.')

    if (numbers.length < 2 || numbers.length > 4) {
      throw new IllegalArgumentException("Illegal version string provided")
    }

    try {
      val major = numbers(0).toInt
      val minor = numbers(1).toInt

      try {
        if (numbers.length > 2) {
          val sprint = numbers(2).toInt
          val build = numbers(3).toInt

          VersionEntry(major, minor, sprint, build)
        } else {
          VersionEntry(major, minor, 0, 0)
        }
      } catch {
        case _: NumberFormatException =>
          VersionEntry(major, minor, -1, -1)
      }
    } catch {
      case e: NumberFormatException =>
        throw new IllegalArgumentException(e)
    }
  }
}
