#!groovy

//
// ~ Copyright © 2018 Aram Meem Company Limited.  All Rights Reserved.
//

currentBuild.result = "SUCCESS"

try {
  def app
  def scmBranch = "${env.BRANCH_NAME}"
  def buildBranches = ~/(dev|master)/
  def testBranches = ~/feature\/CHAT-[0-9]{1,50}/
  def tagBranch = "dev"
  def dockerDateTag
  def lastNonMergedCommit
  def targetStack
  def versionFileName = "chat-websocket"
  def awsRegion = "eu-west-1"

  // define array of stacks
  def stacks = ['bedev','sdkdev','sandbox','qa01','qa05']

  // define stack with nonautodeploy(only send versions to S3)
  def nonAutoStack = ~/(sdkdev|qa01|sandbox)/

  node('jenkins-scala') {

    if (( scmBranch ==~ buildBranches) || (scmBranch ==~ testBranches)) {
      stage('Clone repository') {
        checkout scm
        lastNonMergedCommit = sh(returnStdout:true, script: "git rev-list --max-count=1 --no-merges HEAD").trim()
        currDate = sh(returnStdout:true, script: 'date +%Y%m%d%H%M%S').trim()
        dockerDateTag=currDate + "-" + lastNonMergedCommit.take(6)

      }

      container('scala'){
        stage('Compile scala'){
          sh 'sbt package'
          sh 'sbt "testOnly com.redmadrobot.tests.UnitTests com.redmadrobot.tests.JwtTests"'
          sh 'cp -r /home/jenkins/.ivy2/cache .'
        }
      }
    }

    if ( scmBranch ==~ buildBranches){

    if ( scmBranch == tagBranch) {
      stage('Create tag') {
        withCredentials([string(credentialsId: 'gh_token_amjx', variable: 'GH_TOKEN')]) {
          checkout scm

          sh(returnStdout: false, script: "git fetch https://amjx-bot:${env.GH_TOKEN}@github.com/arammeem/chat-websocket.git --tags")
          def currentTagName = sh(returnStdout: true, script: "git describe --abbrev=0").trim()

          def version = "grep 'version := ' build.sbt | grep -o -Po '\\d{1,4}.\\d{1,4}.\\d{1,4}.\\d{1,4}'"
          def major = sh(returnStdout: true, script: "${version} | cut -d'.' -f1").trim()
          def minor = sh(returnStdout: true, script: "${version} | cut -d'.' -f2").trim()
          def newTagName = "${major}.${minor}"

          if (currentTagName != newTagName) {
            sh("git push https://amjx-bot:${env.GH_TOKEN}@github.com/arammeem/chat-websocket.git :refs/tags/${newTagName}")
            sh("git tag -fa ${newTagName} --message='${newTagName}'")
            sh("git push https://amjx-bot:${env.GH_TOKEN}@github.com/arammeem/chat-websocket.git ${newTagName}")
          }
        }
      }
    }

    container('jx-base') {
    stage('Build image') {
    	app = docker.build("arammeem16/chat-websocket:$lastNonMergedCommit")
    }

    stage('Push image') {
      app.push()
      app.push("latest")
    }


    // avoid autodeploy from master branch
    if ( scmBranch != "master"){

    stage('Deploy image'){
        println "Going to deploy new version of image \"$lastNonMergedCommit\""
        // Prepare image_hash json file
        sh  " echo \'{\"CHAT_WEBSOCKET_HASH\": \"$lastNonMergedCommit\"}\' > /tmp/newVer.json"

        // Deploy on predefined stacks
        for (x in stacks) {
          def stack = x
          sh(returnStdout:true, script: "aws s3 cp /tmp/newVer.json s3://arm-conf/chat/${stack}/versions/${versionFileName}.json").trim()
          if ( !(stack ==~ nonAutoStack) ){
            println "Deploying to stack $stack ..."
            withCredentials([string(credentialsId: '00000003', variable: 'TOKEN')]) {
              sh " curl -v --user ${TOKEN} -X PUT -d \"`date +%s`\" \"https://deploy-${stack}.chat.arammeem.net/v1/kv/run-cox/deploy?dc=substrate&raw\" "
            }
          }
        }
      }
    }

    }

  }
  }
}
catch (err) {
  currentBuild.result = "FAILURE"
  throw err
}
