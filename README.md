# WebSocket proxy application readme

## Requirements

1) Scala >2.12.7;
2) Java >8;
3) SBT (Scala Build Tool) >1.2.6.

> This application uses SBT (Scala Build Tool).
Use provided help to pass arguments and configure new instance.

## How to run application

1) To print help menu: `sbt "run --help"`;
2) To run this application you have to options:
    1. `sbt run` with additional arguments, for example `sbt "run -h localhost -p 8888 -k kafka-host:9092"`
    2. Explicitly pass parameters to jvm (not recommended): `java -Xms4G -Xmx4G -XX:ReservedCodeCacheSize=128m -XX:MaxMetaspaceSize=256m -XX:+UseParallelGC -Dscala.ext.dirs=/home/ec2-user/.sbt/0.13/java9-rt-ext-oracle_corporation_11 -jar /usr/share/sbt/bin/sbt-launch.jar "run -h 10.128.1.223 -p 8888 -k 127.0.0.1:9092"`
3) To run tests: `sbt "testOnly com.redmadrobot.tests.Tests -- -Dh=localhost -Dp=8888 -Dt=30 -Dn=300"`
4) To run test data generator: `sbt "testOnly com.redmadrobot.tests.TestDataGenerator -- -Df=somefile.json -Dh=localhost -Dp=8888"`

## Available configuration parameters
| Parameter name                    | Default value   | Description                                                |
|-----------------------------------|-----------------|------------------------------------------------------------|
| -h --host                         | localhost       | Host or ip addr to bind to.                                |
| -p --port                         | 8080            | Port to bind to.                                           |
| -k --kafka                        | kafka-host:9092 | Kafka broker list.                                         |
| -n --part                         | 0               | Load test time in seconds.                                 |
| -t --timeout                      | 5000            | Kafka partition assignment timeout.                        |
| -s --secure                       | false           | Use secure WebSocket over TLS.                             |
| -b --bound                        | 1000            | Maximum client offset lag before receiving error response. |
| -l --linger                       | 100             | Linger in milliseconds between consumer task state check.  |
| --redis-host                      | 10.110.10.185   | Host of redis.                                             |
| --redis-port                      | 6379            | Port of redis.                                             |
| --redis-connection-timeout        | 2000            | Redis connection timeout.                                  |
| --chat-backend-host               | 10.110.10.185   | Host of chat-backend application.                          |
| --chat-backend-port               | 8080            | Port of chat-backend application.                          |
| --http-client-connection-timeout  | 1000            | Default http client timeout in milliseconds.               |
* linger option allows to drop/configure CPU utilization, but can increase delay on first request.

## Available test configuration parameters
| Parameter name | Default value                        | Description                                  |
|----------------|--------------------------------------|----------------------------------------------|
| -h --host      | localhost                            | Host or ip addr to connect to.               |
| -p --port      | 8080                                 | Port to connect to.                          |
| -c --conn      | 30000                                | Connection timeout in milliseconds.          |
| -t --time      | 60                                   | Load test time in seconds.                   |
| -n --clients   | 60                                   | Number of clients in connection/load tests   |
| -d --delay     | 0                                    | Delay between requests in milliseconds.      |
| -f --file      | "src/test/resources/test-data.json"  | Test data file name for test data generator. |
| -s --secure    | false                                | Set to use HTTPS protocol.                   |
| -a --append    | Empty string                         | Append suffix/destination to connection URI. |
| -u --user      | 5fc82890-52f2-4839-9ce9-a57c4cb43ccc | Test data generator user id.                 |

### Test data generator description
Test data generator allows you to create prepared conversations with messages with current author user id for specific
host.
For example:  `sbt "testOnly com.redmadrobot.tests.TestDataGenerator -- -Dh=node.ab.arammeem.net -Dp=8888 -Dn=5 -Dc=45000 -Dt=30 -Da="ws"`
This command will launch test data generator that will try to receive current conversations,
provide diff between them and create missing conversations.

## How to add S3 credentials for using of S3-based maven repository
1. Go to [https://console.aws.amazon.com/iam/home?region=us-east-2#/users](https://console.aws.amazon.com/iam/home?region=us-east-2#/users) and open your user.
2. On **Security credentials** tab, generate your permanent access key, by pressing **Create access key** button. Permanent credentials is using only for generation of temporary credentials.
3. Install **aws cli**. You can find instruction on [https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
4. Configure aws util with command: `$ aws configure`. You have to use permanent credentials, which you generated at step 2.
5. Assign MFA device, using instruction [https://aws.amazon.com/premiumsupport/knowledge-center/authenticate-mfa-cli/](https://aws.amazon.com/premiumsupport/knowledge-center/authenticate-mfa-cli/)
6. Optional step. You can add named profile for permanent credentials, using instruction [https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)
7. Generate temporary credentials with command `aws sts get-session-token`. Temporary credentials are used for interaction with AWS. Example of command using: `aws sts get-session-token --profile getkey --serial-number your_arn_aws_iam --duration-seconds 129600 --token-code 000000`
8. Add generated temporary credentials to your `~/.aws/credentials` File may looks like this:
```
~/.aws > cat ~/.aws/credentials
[getkey]
aws_secret_access_key=3zUIxJBlg4+X1yIeroxLbX8OYLsV4K334
aws_access_key_id=AKIRYWI4Q7FZSQ6QTE
region=eu-central-1
output=json

[default]
aws_secret_access_key=oGH7W5Uv5avRFFQHlelAh23ELijU5bPLXHb
aws_session_token=FQoGZXIvYXdzEO3//////////wEaDFtJU2kyxmR8u4Qp70j2YNNJpsFAHsO9d2RXH5M05+0Eh6+WGe0ui1rdwso6EzOqmu87Yjl/AtdxzeRuiPdVy6yyYffvGNkUIlb2kZdMHeFetwp+IZx8AlG3Nv08vAvaykyOWkLnWdRRBqdE9vL09BiqGAnQ6CXHo7tL6Rc1TKMHxnuAF
aws_access_key_id=ASIA2STK3RJ5MPCV
region=eu-central-1
output=json
```