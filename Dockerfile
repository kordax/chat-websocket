FROM openjdk:11

# Env variables
ENV SBT_VERSION 1.2.3

# Install sbt
RUN \
 echo "deb https://dl.bintray.com/sbt/debian /" |  tee -a /etc/apt/sources.list.d/sbt.list && \
 apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
 apt-get update &&  apt-get install -y \
 apt-utils sbt=$SBT_VERSION dnsutils net-tools netcat unzip zip bzip2 gnupg curl wget jq \
 python-pip python-setuptools && pip install --upgrade 'pip==9.0.3' && \
 pip install j2cli && \
 pip install yq && \
 mkdir /app

# Define working directory
WORKDIR /app

COPY . /app/
#move cache from Jenkins job
RUN mkdir -p /root/.ivy2 && mv /app/cache /root/.ivy2/cache && \
    /bin/bash -c 'rm src/main/resources/*-{dev,test}.{xml,properties}' 2>/dev/null || true ; \
    mv src/main/resources/logback-docker.xml src/main/resources/logback.xml

EXPOSE 8888

ENTRYPOINT ["/app/docker-files/entrypoint.sh"]
