name := "chat-websocket"
version := "0.9.66.0"
scalaVersion := "2.12.8"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
resolvers += "Arm maven repo" at "s3://arm-maven-repo"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.19",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.19" % Test,
  "com.typesafe.akka" %% "akka-http" % "10.1.6",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.6" % Test,
  "com.typesafe.akka" %% "akka-stream" % "2.5.19",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.19" % Test
)

libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies += "com.thesamet.scalapb" %% "scalapb-runtime" % "0.8.1"
libraryDependencies += "commons-codec" % "commons-codec" % "1.11"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.0.0"
libraryDependencies += "io.spray" %%  "spray-json" % "1.3.4"
libraryDependencies += "com.thesamet.scalapb" %% "scalapb-json4s" % "0.7.1"
libraryDependencies += "net.logstash.logback" % "logstash-logback-encoder" % "5.2"
libraryDependencies += "com.thedeanda" % "lorem" % "2.1"
libraryDependencies += "com.redmadrobot" % "chat-protobuf_2.12" % "0.10.66.1"
libraryDependencies += "redis.clients" % "jedis" % "3.0.0"
libraryDependencies += "com.thesamet.scalapb" %% "scalapb-json4s" % "0.7.1"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.6"

lazy val root = (project in file(".")).
  enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "buildinfopkg"
  )
