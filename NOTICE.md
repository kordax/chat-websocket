# NOTICE

Copyright 2019 Aram Meem Company Limited

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Licenses
Parts of this software are provided under separate licenses.

### com.github.scopt
This software also uses portions of the
`scopt/scopt`
project, which is MIT licensed with the following copyright:

> This project is licensed under the MIT license.
> 
> Copyright (c) scopt contributors
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

A copy of the [MIT] license is provided as part of this distribution.

### logback-classic
This software also uses portions of the
`qos-ch/logback`
project, which is MIT licensed with the following copyright:

> Logback LICENSE
> ---------------
> 
> Logback: the reliable, generic, fast and flexible logging framework.
> Copyright (C) 1999-2015, QOS.ch. All rights reserved.
> 
> This program and the accompanying materials are dual-licensed under
> either the terms of the Eclipse Public License v1.0 as published by
> the Eclipse Foundation
>  
>   or (per the licensee's choosing)
>  
> under the terms of the GNU Lesser General Public License version 2.1
> as published by the Free Software Foundation.

A copy of the [EPL-1.0] license is provided as part of this distribution.

### scala-logging
This software also uses portions of the
`lightbend/scala-logging`
project, which is Apache-2.0 licensed with the following copyright:

> This software is licensed under the Apache 2 license, quoted below.
> 
> Licensed under the Apache License, Version 2.0 (the "License"); you may not
> use this file except in compliance with the License. You may obtain a copy of
> the License at
> 
>     [http://www.apache.org/licenses/LICENSE-2.0]
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
> WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
> License for the specific language governing permissions and limitations under
> the License.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### Scalactic ScalaTest
This software also uses portions of the
`scalatest/scalatest`
project, which is Apache-2.0 licensed with the following copyright:

> /*
> * Copyright 2001-2013 Artima, Inc.
> *
> * Licensed under the Apache License, Version 2.0 (the "License");
> * you may not use this file except in compliance with the License.
> * You may obtain a copy of the License at
> *
> *     http://www.apache.org/licenses/LICENSE-2.0
> *
> * Unless required by applicable law or agreed to in writing, software
> * distributed under the License is distributed on an "AS IS" BASIS,
> * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> * See the License for the specific language governing permissions and
> * limitations under the License.
  */

NOTICE file contents:

> ================================================================================
> ==    NOTICE file corresponding to section 4(d) of the Apache License,        ==
> ==    Version 2.0, in this case for the ScalaTest distribution.               ==
> ================================================================================
> 
>   - This product includes software developed by
      Artima, Inc. (http://www.artima.com/).

No substantial changes to the software were made.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### ScalaPB
This software also uses portions of the
`scalapb/ScalaPB`
project, which is Apache-2.0 licensed with no copyright.

No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### Apache Commons Codec
This software also uses portions of the
`apache/commons-codec`
project, which is Apache-2.0 licensed with the following copyright:

> Apache Commons Codec
> Copyright 2002-2017 The Apache Software Foundation
>
> This product includes software developed at
> The Apache Software Foundation (http://www.apache.org/).
> 
> src/test/org/apache/commons/codec/language/DoubleMetaphoneTest.java
> contains test data from http://aspell.net/test/orig/batch0.tab.
> Copyright (C) 2002 Kevin Atkinson (kevina@gnu.org)
> 
> ===============================================================================
> 
> The content of package org.apache.commons.codec.language.bm has been translated
> from the original php source code available at http://stevemorse.org/phoneticinfo.htm
> with permission from the original authors.
> Original source copyright:
> Copyright (c) 2008 Alexander Beider & Stephen P. Morse.

NOTICE file contents are same as copyright.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### Apache Kafka
This software also uses portions of the
`apache/kafka`
project, which is Apache-2.0 licensed with the following copyright:

> Apache Kafka
> Copyright 2018 The Apache Software Foundation.
> 
> This product includes software developed at
> The Apache Software Foundation (http://www.apache.org/).
> 
> This distribution has a binary dependency on jersey, which is available under the CDDL
> License. The source code of jersey can be found at https://github.com/jersey/jersey/.

No substantial changes to the software were made.

A copy of the [Apache-2.0] license is provided as part of this distribution.  

### Spray JSON
This software also uses portions of the
`spray/spray-json`
project, which is Apache-2.0 licensed with the following copyright:

> /*
>  * Original implementation (C) 2009-2011 Debasish Ghosh
>  * Adapted and extended in 2011 by Mathias Doenitz
>  *
>  * Licensed under the Apache License, Version 2.0 (the "License");
>  * you may not use this file except in compliance with the License.
>  * You may obtain a copy of the License at
>  *
>  * http://www.apache.org/licenses/LICENSE-2.0
>  *
>  * Unless required by applicable law or agreed to in writing, software
>  * distributed under the License is distributed on an "AS IS" BASIS,
>  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  * See the License for the specific language governing permissions and
>  * limitations under the License.
   */

No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### ScalaPB json4s
This software also uses portions of the
`scalapb/scalapb-json4s`
project, which is Apache-2.0 licensed with the following copyright:

No copyright was provided.
No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### Logback JSON encoder
This software also uses portions of the
`logstash/logstash-logback-encoder`
project, which is Apache-2.0 licensed with the following copyright:

No copyright was provided.
No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [Apache-2.0] license is provided as part of this distribution.

### mdeanda/lorem
This software also uses portions of the
`mdeanda/lorem`
project, which is MIT licensed with the following copyright:

> Copyright (c) 2015 Miguel De Anda

No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [MIT] license is provided as part of this distribution.

### AKKA Framework
This software also uses portions of the
`akka/akka`
project, which is Apache-2.0 licensed with the following copyright:

> /**
>  * Copyright (C) 2009-2018 Lightbend Inc. <https://www.lightbend.com>
   */

No substantial changes to the software were made.
No `NOTICE` file was provided.

A copy of the [Apache-2.0] license is provided as part of this distribution.

[MIT]: licenses/MIT
[GPL-2.0]: licenses/GPL-2.0
[Apache-2.0]: licenses/APL-2.0
[MPL-1.1]: licenses/MPL-1.1
[EPL-2.0]: licenses/EPL-2.0
[CDDL 1.1]: licenses/CDDL-1.1
[BSD 2-Clause]: licenses/BSD2-Clause
[BSD 3-Clause]: licenses/BSD3-Clause